import numpy as np
import cv2
import os
from glob import glob
import RVLPYDDDetectorLHTCP as rvl

print('Starting...')

# root_path = '/home/RVLuser/data/Exp-LHTCP_K07-20240430/Exp-K07-20240430-4'
# root_path = '/home/RVLuser/data/Exp-LHTCP_K07-20240424'
# root_path = '/home/RVLuser/data/Exp-LHTCP_K07-20240506/Exp-LHTCP_K07-20240430-4'
root_path = '/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619'

lhtcp_save_path = os.path.join(root_path, 'results', 'lhtcp_pose_est')
detector = rvl.PYDDDetectorLHTCP()
detector.create('/home/RVLuser/rvl-linux/RVLRecognitionDemo_Simundic_DDD_3D_to_2D_fitting_LHTCP.cfg')

cameras_params_dir = os.path.join(root_path, 'camera_config', 'mono')
extrinsic_camera_params_dir = os.path.join(root_path, 'camera_config', 'stereo')
detector.load_cameras_parameters(cameras_params_dir)
detector.load_extrinsic_camera_parameters(extrinsic_camera_params_dir)
detector.create_lhtcp_model(0.015, 0.1, 0.005, 0.7)
detector.generate_init_poses(20.0)

rgb_images_dir = os.path.join(root_path, 'results', 'images')
rgb_images_est_dir = os.path.join(root_path, 'results', 'images_est')

rgb_images_prefix = 0

for i in range(len(glob(os.path.join(rgb_images_dir, '*-0.png')))):

    rgb_image0_filename = os.path.join(rgb_images_dir, '%d-0.png' % i)
    rgb_image1_filename = os.path.join(rgb_images_dir, '%d-1.png' % i)
    detector.add_rgb_images(rgb_image0_filename, rgb_image1_filename)

    T_M_C, solution0, solution1 = detector.detect()

    np.save(os.path.join(lhtcp_save_path, 'T_M_C0_%d.npy' % i), T_M_C)

    cv2.imwrite(os.path.join(rgb_images_est_dir, '%d-0.png' % i), solution0)
    cv2.imwrite(os.path.join(rgb_images_est_dir, '%d-1.png' % i), solution1)

    print(T_M_C)


cv2.imshow('Image0', solution0)
cv2.imshow('Image1', solution1)
cv2.waitKey(0)


print('completed.')