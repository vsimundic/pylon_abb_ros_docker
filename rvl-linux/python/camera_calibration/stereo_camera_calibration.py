import numpy as np
import cv2
import glob
import os
import pickle
np.set_printoptions(suppress = True)

chessboard_size = (8, 6)
square_size = 0.01
camera_data_path = '/home/RVLuser/data/Exp-CameraCalibration-20240502/camera_config/stereo'
intrinsic_camera_data_path = '/home/RVLuser/data/Exp-CameraCalibration-20240502/camera_config/mono'
test_path_dir = os.path.join(camera_data_path, 'test_calibration_data')
# camera_data_path = '/home/RVLuser/data/Exp-LHTCP_K07-20240430/Exp-LHTCP_K07-20240430-1/camera_config/stereo'
# intrinsic_camera_data_path = '/home/RVLuser/data/Exp-LHTCP_K07-20240430/Exp-LHTCP_K07-20240430-1/camera_config/mono'
suffix = ''
# suffix = '_rvlpy'
test_calib = True
load_calib = False
show_projected_images = False

T_C1_C0_init = np.array([[0., 0., -1., 0.4],
                         [0., 1., 0., 0.],
                         [1., 0., 0., 0.6],
                         [0., 0., 0., 1.]])



fs = cv2.FileStorage(os.path.join(intrinsic_camera_data_path, 'camera0_params%s.yaml' % suffix), cv2.FILE_STORAGE_READ)
camera0_mtx = fs.getNode('camera_matrix').mat()
camera0_dst = fs.getNode('distortion_coefficients').mat()
fs.release()
fs = cv2.FileStorage(os.path.join(intrinsic_camera_data_path, 'camera1_params%s.yaml' % suffix), cv2.FILE_STORAGE_READ)
camera1_mtx = fs.getNode('camera_matrix').mat()
camera1_dst = fs.getNode('distortion_coefficients').mat()
fs.release()

if not load_calib:

    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    objp = np.zeros((chessboard_size[0]*chessboard_size[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2) * square_size

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints0 = []  # 2D points in left image plane
    imgpoints1 = []  # 2D points in right image plane

    images0 = sorted(glob.glob(os.path.join(camera_data_path, 'images', '*-0.png')))
    images1 = sorted(glob.glob(os.path.join(camera_data_path, 'images', '*-1.png')))

    images0_detected = []
    images1_detected = []

    for image0_path, image1_path in zip(images0, images1):
        image0 = cv2.imread(image0_path)
        image1 = cv2.imread(image1_path)

        image0 = cv2.undistort(image0, camera0_mtx, camera0_dst)
        image1 = cv2.undistort(image1, camera1_mtx, camera1_dst)

        image0_gray = cv2.cvtColor(image0, cv2.COLOR_BGR2GRAY)
        image1_gray = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)

        # Find the chessboard corners in both images
        ret0, corners0 = cv2.findChessboardCorners(image0_gray, chessboard_size, flags = cv2.CALIB_CB_ADAPTIVE_THRESH | cv2.CALIB_CB_NORMALIZE_IMAGE)
        ret1, corners1 = cv2.findChessboardCorners(image1_gray, chessboard_size, flags = cv2.CALIB_CB_ADAPTIVE_THRESH | cv2.CALIB_CB_NORMALIZE_IMAGE)

        if ret0 and ret1:
            objp = np.zeros((chessboard_size[0]*chessboard_size[1], 3), np.float32)
            objp[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2) * square_size
            objpoints.append(objp)

            corners0_2 = cv2.cornerSubPix(image0_gray, corners0, (11, 11), (-1, -1), criteria)
            imgpoints0.append(corners0_2)
            corners1_2 = cv2.cornerSubPix(image1_gray, corners1, (11, 11), (-1, -1), criteria)
            imgpoints1.append(corners1_2)

            image0 = cv2.drawChessboardCorners(image0, chessboard_size, corners0_2, ret0)
            image1 = cv2.drawChessboardCorners(image1, chessboard_size, corners1_2, ret1)
            # cv2.imshow('Image 0', image0)
            # cv2.imshow('Image 1', image1)
            # cv2.waitKey(0)

            images0_detected.append(image0_path)
            images1_detected.append(image1_path)

    dist0 = np.zeros((5,))

    T_C0_C1_init = np.linalg.inv(T_C1_C0_init)
    R_init = T_C0_C1_init[:3, :3]
    t_init = T_C0_C1_init[:3, 3].reshape((3,1))
    # R = np.zeros((3,3))
    # T = np.zeros((3,3))
    E_init = np.zeros((3,3))
    F_init = np.zeros((3,3))
    # ret, _, _, _, _, R, t, E, F = cv2.stereoCalibrate(objpoints, imgpoints0, imgpoints1, camera0_mtx, camera0_dst, camera1_mtx, camera1_dst, image0_gray.shape[::-1])
    # ret, _, _, _, _, R, t, E, F = cv2.stereoCalibrate(objpoints, imgpoints0, imgpoints1, camera0_mtx, dist0, camera1_mtx, dist0, image0_gray.shape[::-1])
    # ret, newmtx0, newdist0, newmtx1, newdist1, R, t, E, F, rvecs, tvecs, rmse = cv2.stereoCalibrateExtended(objpoints, imgpoints0, imgpoints1, camera0_mtx, dist0, camera1_mtx, dist0, image0_gray.shape[::-1], R, T, E, F, flags=cv2.CALIB_FIX_INTRINSIC)
    ret, newmtx0, newdist0, newmtx1, newdist1, R, t, E, F, rvecs, tvecs, rmse = cv2.stereoCalibrateExtended(objpoints, 
                                                                                                            imgpoints0, 
                                                                                                            imgpoints1, 
                                                                                                            camera0_mtx, 
                                                                                                            dist0, 
                                                                                                            camera1_mtx, 
                                                                                                            dist0, 
                                                                                                            image0_gray.shape[::-1], 
                                                                                                            R_init, 
                                                                                                            t_init, 
                                                                                                            E_init, 
                                                                                                            F_init, 
                                                                                                            flags=cv2.CALIB_FIX_INTRINSIC | cv2.CALIB_USE_EXTRINSIC_GUESS)

    if not os.path.exists(test_path_dir):
        os.makedirs(test_path_dir)


    TC0C1 = np.eye(4)
    TC0C1[:3, :3] = R
    TC0C1[:3, 3] = t.reshape((3,))
    TC1C0 = np.linalg.inv(TC0C1)

    print(TC1C0)

    np.save(os.path.join(test_path_dir, 'R.npy'), R)
    np.save(os.path.join(test_path_dir, 't.npy'), t)
    np.save(os.path.join(test_path_dir, 'rvecs.npy'), rvecs)
    np.save(os.path.join(test_path_dir, 'tvecs.npy'), tvecs)
    np.save(os.path.join(camera_data_path, 'TC1C0.npy'), TC1C0)

    fs = cv2.FileStorage(os.path.join(camera_data_path, 'TC1C0.yaml'), flags=1)
    fs.write(name='TC1C0', val=TC1C0)
    fs.release()


    with open(os.path.join(test_path_dir, 'images0_detected_list'), 'wb') as f:
        pickle.dump(images0_detected, f)
    with open(os.path.join(test_path_dir, 'images1_detected_list'), 'wb') as f:
        pickle.dump(images1_detected, f)
else:
    R = np.load(os.path.join(test_path_dir, 'R.npy'))
    t = np.load(os.path.join(test_path_dir, 't.npy'))
    rvecs = np.load(os.path.join(test_path_dir, 'rvecs.npy'))
    tvecs = np.load(os.path.join(test_path_dir, 'tvecs.npy'))
    TC1C0 = np.load(os.path.join(camera_data_path, 'TC1C0.npy'))
    with open(os.path.join(test_path_dir, 'images0_detected_list'), 'rb') as f:
        images0_detected = pickle.load(f)
    with open(os.path.join(test_path_dir, 'images1_detected_list'), 'rb') as f:
        images1_detected = pickle.load(f)
        
if test_calib == True:
    save_images0_dir = os.path.join(test_path_dir, 'images0')
    save_images1_dir = os.path.join(test_path_dir, 'images1')
    if not os.path.exists(save_images0_dir):
        os.makedirs(save_images0_dir)
    if not os.path.exists(save_images1_dir):
        os.makedirs(save_images1_dir)

    objp = np.zeros((chessboard_size[0]*chessboard_size[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2) * square_size
    objp_ = np.hstack((objp, np.ones((objp.shape[0],1))))
    
    for i_img in range(len(images0_detected)):

        R_ = cv2.Rodrigues(rvecs[i_img].reshape((3,)))[0]
        T0_ = np.eye(4)
        T0_[:3,:3] = R_
        T0_[:3,3] = tvecs[i_img].reshape((3,))

        objp_0_ = T0_[np.newaxis, ...]  @ objp_[..., np.newaxis]
        objp_0 = objp_0_[:, :-1, :]
        objp_0_img = (camera0_mtx[np.newaxis, ...] @ objp_0).reshape((objp_0.shape[0], 3))
        objp_0_img = objp_0_img[:] / objp_0_img[:, -1][..., np.newaxis]
        objp_0_img = objp_0_img[:, :-1].astype(int)

        objp_1_ = np.linalg.inv(TC1C0)[np.newaxis, ...] @ objp_0_
        objp_1 = objp_1_[:, :-1, :]
        objp_1_img = (camera1_mtx[np.newaxis, ...] @ objp_1).reshape((objp_1.shape[0], 3))
        objp_1_img = objp_1_img[:] / objp_1_img[:, -1][..., np.newaxis]
        objp_1_img = objp_1_img[:, :-1].astype(int)

        image0 = cv2.imread(images0_detected[i_img])
        image0 = cv2.undistort(image0, camera0_mtx, camera0_dst)
        image1 = cv2.imread(images1_detected[i_img])
        image1 = cv2.undistort(image1, camera1_mtx, camera1_dst)

        for i_pt in range(objp_0_img.shape[0]):
            pt0_ = tuple(objp_0_img[i_pt])
            cv2.drawMarker(image0, objp_0_img[i_pt], (0,255,0), markerType=cv2.MARKER_CROSS)

            pt1_ = tuple(objp_1_img[i_pt])
            cv2.drawMarker(image1, objp_1_img[i_pt], (0,255,0), markerType=cv2.MARKER_CROSS)
        
        if show_projected_images:
            cv2.imshow('Image 0', image0)
            cv2.imshow('Image 1', image1)
            cv2.waitKey(0)

        cv2.imwrite(os.path.join(save_images0_dir, '%d.png' % i_img), image0)
        cv2.imwrite(os.path.join(save_images1_dir, '%d.png' % i_img), image1)

        # for pt in imgpts_.squeeze():
        #     pt_ = tuple(pt.astype(int))
        #     cv2.drawMarker(img_, pt_, (0,255,0), markerType=cv2.MARKER_CROSS)


        # p_0 = np.append(tvecs[i], 1)
        # p_1_ = np.linalg.inv(TC1C0) @ p_0
        # p_1 = p_1_[:-1]
        # p1_img = camera1_mtx @ p_1
        # p1_img /= p1_img[-1]
        # p0_img = camera0_mtx @ tvecs[i]
        # p0_img /= p0_img[-1]



        