# Camera calibration
Camera calibration is done with Python scripts stored in `rvl-linux/python/camera_calibration`. 

## Intrinsic camera calibration
Intrinsic camera calibration can be carried out in two ways:
1. ROS intrinsic camera calibration
2. OpenCV intrinsic camera calibration

Both ways should give similar if not the same result. ROS is prefered for taking images.

### 1. ROS intrinsic camera calibration

If calibrating with ROS, follow these steps:
1. Run the camera drivers:

For Asus Xtion:
```bash
roslaunch openni2_launch openni2.launch depth_registration:=true
```
For Intel Realsense:
```bash
roslaunch realsense2_camera rs_camera.launch enable_pointcloud:=true depth_width:=640 depth_height:=480 depth_fps:=15 color_width:=640 color_height:=480 color_fps:=15 align_depth:=true
```

2. Run the calibration program with the command below. Change the topics according to your camera, calibration panel size and the square size in meters. In order to get a good calibration, you will probably need around 70 to 80 images (you can see the number of images in the terminal). Move the checkerboard around in the camera frame such that [[source](https://wiki.ros.org/camera_calibration/Tutorials/MonocularCalibration)]:
  - checkerboard is on the camera's left, right, top and bottom of field of view
    - X bar - left/right in field of view
	- Y bar - top/bottom in field of view
	- Size bar - toward/away and tilt from the camera
  - checkerboard is filling the whole field of view
  - checkerboard is tilted to the left, right, top and bottom (Skew)
  
At each step, hold the checkerboard still until the image is highlighted in the calibration window. As you move the checkerboard around you will see three bars on the calibration sidebar increase in length. When the CALIBRATE button lights, you have enough data for calibration and can click CALIBRATE to see the results. Try to fill all bars as much as you can.

Calibration can take about a minute. The windows might be greyed out but just wait, it is working. 

```bash
rosrun camera_calibration cameracalibrator.py --size 8x6 --square 0.108 image:=/camera/image_raw camera:=/camera --no-service-check

```

3. After the calibration is complete you will see the calibration results in the terminal and the calibrated image in the calibration window. A successful calibration will result in real-world straight edges appearing straight in the corrected image. You can click save to save the images and the calibration files. The path to the files is printed out in the terminal. You can move them using the `mv` command in the terminal.

### 2. OpenCV intrinsic camera calibration
If calibrating with OpenCV, you can use Python scripts in `rvl-linux/python/camera_calibration`. Python script `intrinsic_camera_calibration.py` can be used for intrinsic calibration. 

1. Modify `camera_data_path` to your root path. Modify `images_folder` to your directory for images. The images should be saved in `<camera_data_path>/<images_folder>`.
2. Set the square size of the calibration panel and the chessboard size.
3. Set the `test_camera_calib` variable if you want to test the calibration. 
4. Set the `camera_id` variable according to your camera. If you don't have an ID, set it 0 or any other number. It is just used for saving paths.
5. Run the script. The intrinsics and some other info will be saved to the root path.



## Stereo camera calibration
Stereo calibration is performed with OpenCV. You can use Python scripts in `rvl-linux/python/camera_calibration` for calibration. Python script `stereo_camera_calibration.py` can be used for stereo calibration. 

### Image capture
You can capture images any way you want. They need to be saved in the same folder named `images` with the following format: `<image_number>-<camera_id>.png`. For example, the first pair of images will be named 0-0.png and 0-1.png, the second will be named 1-0.png and 1-1.png etc. The captured images should NOT be undistorted. This will be done by the calibration script with the intrinsic parameters. 

### Calibration process
1. Modify `camera_data_path` to your root path.
2. Modify `intrinsic_camera_data_path` to where your intrinsic calibrations are. Note that the intrinsic parameters files need to be saved as OpenCV YAML files under names `camera<ID>_params.yaml`. Omit the symbols "<>".
3. Set the square size of the calibration panel and the chessboard size.
4. Set the `test_calib` variable if you want to test the calibration. 
4. Set the `load_calib` variable if you want to load the already calibrated matrices. 
5. Set the `show_projected_images` variable if you want to show the images of the projection points. 
6. Set the `T_C1_C0_init` matrix as an initial estimate of the calibration result, where the matrix represents the transformation of the second camera (C1) with respect to the first camera (C0). This is an approximation matrix, so it does not have to be perfectly precise, but has to be in the vicinity of the correct solution.
5. Set the `camera_id` variable according to your camera. If you don't have an ID, set it 0 or any other number. It is just used for saving paths.
5. Run the `stereo_camera_calibration.py` script. The resulting matrix TC1C0 will be saved in the root path under the names `TC10C0.yaml` and `TC10C0.npy`. Extra info about the calibration will be saved in the `test_path_dir` directory. 