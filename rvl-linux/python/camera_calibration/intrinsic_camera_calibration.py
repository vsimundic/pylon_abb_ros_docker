import numpy as np
import cv2
import glob
import os
np.set_printoptions(suppress = True)

camera_id = 1
chessboard_size = (8, 6)
square_size = 0.01
camera_data_path = '/home/RVLuser/data/Exp-CameraCalibration-20240502/camera_config/mono'
images_folder = 'images%d' % camera_id
test_camera_calib = True

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

objp = np.zeros((chessboard_size[0]*chessboard_size[1], 3), np.float32)
objp[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2) * square_size

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob(os.path.join(camera_data_path, images_folder, '*.png'))
detected_img_names = []
intrinsics_in = np.eye(3, dtype=np.float64)
for fname in images:
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, chessboard_size, flags = cv2.CALIB_CB_ADAPTIVE_THRESH |
                                              cv2.CALIB_CB_NORMALIZE_IMAGE)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
    
        corners2 = cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners2)

        detected_img_names.append(fname)

        # # Draw and display the corners
        # cv2.drawChessboardCorners(img, chessboard_size, corners2, ret)
        # cv2.imshow('img', img)
        # cv2.waitKey(0)

# ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], intrinsics_in, None, flags=0)
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
print(mtx)
cv2.destroyAllWindows()

fs = cv2.FileStorage(os.path.join(camera_data_path, 'camera%d_params_rvlpy.yaml' % camera_id), cv2.FileStorage_WRITE)
fs.write('camera_matrix', mtx)
fs.write('distortion_coefficients', dist)

np.save(os.path.join(camera_data_path, 'rvecs%d_rvlpy' % camera_id), rvecs)
np.save(os.path.join(camera_data_path, 'tvecs%d_rvlpy' % camera_id), tvecs)
dist0 = np.zeros((5,))
if test_camera_calib:
    for i, fname in enumerate(detected_img_names):
        objpts_ = np.array(objpoints[i])
        rvecs_ = np.array(rvecs[i])
        tvecs_ = np.array(tvecs[i])
        imgpts_, _ = cv2.projectPoints(objpts_, rvecs_, tvecs_, mtx, dist0)

        # imgpts_undist = cv2.undistortPoints(objpts_, mtx, dist)

        img = cv2.imread(fname)
        img_ = cv2.undistort(img, mtx, dist)

        for pt in imgpts_.squeeze():
            pt_ = tuple(pt.astype(int))
            cv2.drawMarker(img_, pt_, (0,255,0), markerType=cv2.MARKER_CROSS)
        
        cv2.imshow('Image with markers', img_)
        cv2.waitKey(0)

    # for i, fname in enumerate(detected_img_names):
    #     objpts_ = np.array(objpoints[i])
    #     rvecs_ = np.array(rvecs[i])
    #     tvecs_ = np.array(tvecs[i])
    #     imgpts_, _ = cv2.projectPoints(objpts_, rvecs_, tvecs_, mtx, dist)

    #     imgpts_undist = cv2.undistortPoints(objpts_, mtx, dist)

    #     img = cv2.imread(fname)
    #     img_ = cv2.undistort(img, mtx, dist)

    #     for pt in imgpts_undist.squeeze():
    #         pt_ = tuple(pt.astype(int))
    #         cv2.drawMarker(img_, pt_, (0,255,0), markerType=cv2.MARKER_CROSS)
        
    #     cv2.imshow('Image with markers', img_)
    #     cv2.waitKey(0)

