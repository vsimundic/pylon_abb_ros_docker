#include "RVLCore2.h"
#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);
#include "RVLVTK.h"
#include "Util.h"
#include "Space3DGrid.h"
#include "Graph.h"
#include "Mesh.h"
#include "Visualizer.h"
#include "SceneSegFile.hpp"
#include "SurfelGraph.h"
#include "PlanarSurfelDetector.h"
#include "RVLRecognition.h"
#include "RVLRecognitionCommon.h"
#include "Voter.h"
#include "RVLBuffer.h"
#include "RVLPtrChain.h"
#include "RVLMPtrChain.h"
#include "Rect.h"
#include "RVLEDT.h"
#include "DDDetector.h"
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

using namespace RVL;
using namespace py::literals;

////////////////////////////////////////////////////////////////////
//
//     PYDDDetectorLHTCP
//
////////////////////////////////////////////////////////////////////

class PYDDDetectorLHTCP
{
public:
	PYDDDetectorLHTCP();
	~PYDDDetectorLHTCP();
	void create(
		std::string cfgFileName);
	void set_memory_storage_size(
		int mem0Size,
		int memSize);
	void clear();

	void load_cameras_parameters(std::string cameraParamsFolder);
	void load_extrinsic_camera_parameters(std::string extrinsicParamsFolder);
	void create_lhtcp_model(float r_cylinder_top, float h_cylinder_top, float r_cylinder_bottom, float h_cylinder_bottom);
	void generate_init_poses(float angle_deg);
	void add_rgb_images(std::string file_name_0, std::string file_name_1);
	py::tuple detect();

	void visualize_from_pose(py::array T_T_C0_in, py::array T_T_C1_in);

public:
	DDDetector detector;
	int memSize;
	int mem0Size;
	std::vector<Mesh *> meshSeq;
	std::vector<cv::Mat> RGBSeq;
	Camera camera;
	float maxMeshTriangleEdgeLen;
	Visualizer visualizer;
	RECOG::DDD::DisplayCallbackData* pVisualizationData;
	std::string hypFileName;
	cv::Mat *camsMatrix = NULL;
	cv::Mat *camsDist = NULL;
	RVL::Pose3D T_C_C0[2];
	Mesh modelMesh;
	Array<Pose3D> initPoses;
	Pose3D initPosesMem[4];
	cv::Mat RGBs[2];
	cv::Mat RGBsUndistorted[2];
};


PYDDDetectorLHTCP::PYDDDetectorLHTCP()
{
	maxMeshTriangleEdgeLen = 0.050f;	// m
	memSize = 1000000000;
	mem0Size = 1000000000;
}


PYDDDetectorLHTCP::~PYDDDetectorLHTCP()
{
	clear();
}


void PYDDDetectorLHTCP::create(
	std::string cfgFileName)
{
	detector.pMem0 = new CRVLMem;
	detector.pMem0->Create(mem0Size);
	detector.pMem = new CRVLMem;
	detector.pMem->Create(memSize);
	char *cfgFileName_ = (char *)(cfgFileName.data());
	detector.Create(cfgFileName_);
	camsMatrix = new cv::Mat[2];
	camsDist = new cv::Mat[2];

	// LoadCameraParametersFromFile(cfgFileName_, camera, detector.pMem0);
	detector.camera = camera; 
	visualizer.Create();
	detector.InitVisualizer(&visualizer);

	initPoses.Element = initPosesMem;
	initPoses.n = 4;

}


void PYDDDetectorLHTCP::set_memory_storage_size(
		int mem0SizeIn,
		int memSizeIn)
{
	memSize = memSizeIn;
	mem0Size = mem0SizeIn;
}


void PYDDDetectorLHTCP::clear()
{
	delete detector.pMem0;
	delete detector.pMem;
	delete detector.pSurfels;
	delete detector.pSurfelDetector;
	delete[] camsMatrix;
	delete[] camsDist;
}


void PYDDDetectorLHTCP::load_cameras_parameters(std::string cameraParamsFolder)
{
	detector.loadIntrinsicCamerasParams(cameraParamsFolder, camsMatrix, camsDist);
}


void PYDDDetectorLHTCP::load_extrinsic_camera_parameters(std::string extrinsicParamsFolder)
{
	detector.loadBaslerExtrinsicParams(extrinsicParamsFolder, &T_C_C0[0]);
}


void PYDDDetectorLHTCP::create_lhtcp_model(float r_cylinder_top, float h_cylinder_top, float r_cylinder_bottom, float h_cylinder_bottom)
{
	detector.CreateLHTCPModel(&modelMesh, r_cylinder_top, h_cylinder_top, r_cylinder_bottom, h_cylinder_bottom, 20);
	detector.CreateMeshFromPolyData(&modelMesh);
}


void PYDDDetectorLHTCP::generate_init_poses(float angle_deg)
{
	Pose3D poseMCInit0, poseMCInit, poseMC;
	for (int i = 0; i < initPoses.n / 2; i++)
	{
		float th = -0.5f * PI + (float)(2 * i - 1) * angle_deg * DEG2RAD;
		float cs = cos(th);
		float sn = sin(th);
		RVLROTX(cs, sn, poseMCInit0.R);
		RVLSET3VECTOR(poseMCInit0.t, 0.0f, 0.0f, T_C_C0[1].t[2]);
		initPoses.Element[i] = poseMCInit0;
	}
	float th, cs, sn, csx, snx;
	float rotX[9], rotZ[9];
	for (int i = 0; i < initPoses.n / 2; i++) // rotz
	{
		csx = cos(-0.5f * PI);
		snx = sin(-0.5f * PI);
		RVLROTX(csx, snx, rotX);

		th = (float)(2 * i - 1) * angle_deg * DEG2RAD;
		cs = cos(th);
		sn = sin(th);
		RVLROTZ(cs, sn, rotZ);

		RVLMXMUL3X3(rotZ, rotX, poseMCInit0.R);

		RVLSET3VECTOR(poseMCInit0.t, 0.0f, 0.0f, T_C_C0[1].t[2]);
		initPoses.Element[i + 2] = poseMCInit0;
	}
}


void PYDDDetectorLHTCP::add_rgb_images(std::string file_name_0, std::string file_name_1)
{
	RGBs[0] = cv::imread((file_name_0).data());
	RGBs[1] = cv::imread((file_name_1).data());

	// cv::imshow("RGB 0", RGBs[0]);
	// cv::imshow("RGB 1", RGBs[1]);
	// cv::waitKey();
}


py::tuple PYDDDetectorLHTCP::detect()
{
	// cv::Mat RGBsUndistorted[2];
	cv::Mat *solutions;
	solutions = new cv::Mat[2];
	Pose3D poseMC;

	for (int i = 0; i < 2; i++)
	{
		cv::undistort(RGBs[i], RGBsUndistorted[i], camsMatrix[i], camsDist[i]);
	}
	cout << "Before fitting." << endl; 
	detector.Fit3DTo2DStereo(&modelMesh, &RGBsUndistorted[0], 2, initPoses, T_C_C0, poseMC, solutions, 0);
	cout << "Fitted." << endl;
	
	auto T_M_C = py::array(py::buffer_info(
		nullptr,
		sizeof(float),
		py::format_descriptor<float>::value,
		2,
		{4, 4},
		{4 * sizeof(float), sizeof(float)}
	));
	float *T_M_C_ = (float *)T_M_C.request().ptr;

	RVLHTRANSFMX(poseMC.R, poseMC.t, T_M_C_);

	int width, height;
	width = solutions[0].cols;
	height = solutions[0].rows;

	cv::Mat solution0_clone_mat = solutions[0].clone();
	uchar* data_ptr0 = solution0_clone_mat.data;

	auto solution0 = py::array(py::buffer_info(
		// nullptr,
		data_ptr0,
		sizeof(uchar),
		py::format_descriptor<uchar>::value,
		3,
		{height, width, 3},
		{width * 3 * sizeof(uchar), 3 * sizeof(uchar), sizeof(uchar)}));

	// uchar *solution0_ = (uchar *)solution0.request().ptr;
	// solution0_ = solutions[0].data;

	cv::Mat solution1_clone_mat = solutions[1].clone();
	uchar *data_ptr1 = solution1_clone_mat.data;

	auto solution1 = py::array(py::buffer_info(
		// nullptr,
		data_ptr1,
		sizeof(uchar),
		py::format_descriptor<uchar>::value,
		3,
		{height, width, 3},
		{width * 3 * sizeof(uchar), 3 * sizeof(uchar), sizeof(uchar)}));


	py::tuple result_tuple = py::make_tuple(T_M_C, solution0, solution1);

	delete[] solutions;
	cout << "Done." << endl;

	return result_tuple;
}

void PYDDDetectorLHTCP::visualize_from_pose(py::array T_T_C0_in, py::array T_T_C1_in)
{
	Pose3D T_T_C0, T_T_C1, T_C0_C1;
	double *T_T_C0_ = (double *)T_T_C0_in.request().ptr;
	RVLHTRANSFMXDECOMP(T_T_C0_, T_T_C0.R, T_T_C0.t);
	double *T_T_C1_ = (double *)T_T_C1_in.request().ptr;
	RVLHTRANSFMXDECOMP(T_T_C1_, T_T_C1.R, T_T_C1.t);

	// Create edges and compute their lengths.
	uchar red[3] = {255, 0, 0};
	RECOG::DDD::Edge *edge = new RECOG::DDD::Edge[modelMesh.EdgeArray.n];
	RECOG::DDD::Edge *pEdge_;
	MeshEdge *pEdge;
	int iEdge;
	Point *pPt, *pPt_;
	float V3Tmp[3];
	for (iEdge = 0; iEdge < modelMesh.EdgeArray.n; iEdge++)
	{
		pEdge = modelMesh.EdgeArray.Element + iEdge;
		pPt = modelMesh.NodeArray.Element + pEdge->iVertex[0];
		pPt_ = modelMesh.NodeArray.Element + pEdge->iVertex[1];
		pEdge_ = edge + iEdge;
		RVLDIF3VECTORS(pPt_->P, pPt->P, V3Tmp);
		pEdge_->length = sqrt(RVLDOTPRODUCT3(V3Tmp, V3Tmp));
		pEdge_->iVertex[0] = pEdge->iVertex[0];
		pEdge_->iVertex[1] = pEdge->iVertex[1];
	}

	float *PRMem = new float[modelMesh.NodeArray.n * 3];
	float *PCMem = new float[modelMesh.NodeArray.n * 3];
	float *NCMem = new float[modelMesh.faces.n * 3];
	float PRMem_[6 * 3];

	detector.Project3DModelToImage(&modelMesh, T_T_C1, edge, PRMem, PCMem, NCMem, 1);
	detector.Visualize3DModelImageProjection(RGBsUndistorted[1], &modelMesh, edge , red);
	detector.Project3DModelToImage(&modelMesh, T_T_C0, edge, PRMem, PCMem, NCMem, 0);
	detector.Visualize3DModelImageProjection(RGBsUndistorted[0], &modelMesh, edge, red);

	cv::imshow("LHTCP fit " + std::to_string(0), RGBsUndistorted[0]);
	cv::imshow("LHTCP fit " + std::to_string(1), RGBsUndistorted[1]);
	cv::waitKey(0);
	cv::destroyAllWindows();
	cv::imwrite("/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619/0_gt.png", RGBsUndistorted[0]);
	cv::imwrite("/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619/1_gt.png", RGBsUndistorted[1]);

	delete[] PRMem;
	delete[] PCMem;
	delete[] NCMem;
	delete[] edge;
}


////////////////////////////////////////////////////////////////////
//
//     RVL PYDDDetectorLHTCP Wrapper
//
////////////////////////////////////////////////////////////////////

PYBIND11_MODULE(RVLPYDDDetectorLHTCP, m) 
{
	m.doc() = "RVL PYDDDetectorLHTCP wrapper";

	py::class_<PYDDDetectorLHTCP>(m, "PYDDDetectorLHTCP")
		.def(py::init<>())
		.def("create", &PYDDDetectorLHTCP::create)
		.def("set_memory_storage_size", &PYDDDetectorLHTCP::set_memory_storage_size)
		.def("clear", &PYDDDetectorLHTCP::clear)
		.def("load_cameras_parameters", &PYDDDetectorLHTCP::load_cameras_parameters)
		.def("load_extrinsic_camera_parameters", &PYDDDetectorLHTCP::load_extrinsic_camera_parameters)
		.def("create_lhtcp_model", &PYDDDetectorLHTCP::create_lhtcp_model)
		.def("generate_init_poses", &PYDDDetectorLHTCP::generate_init_poses)
		.def("add_rgb_images", &PYDDDetectorLHTCP::add_rgb_images)
		.def("detect", &PYDDDetectorLHTCP::detect)
		.def("visualize_from_pose", &PYDDDetectorLHTCP::visualize_from_pose);
}
