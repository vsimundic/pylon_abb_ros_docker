import numpy as np
import os
import matplotlib.pyplot as plt
import yaml
from yaml.loader import SafeLoader
import cv2
import rvlpyutil as rvl
import robcamcal
np.set_printoptions(suppress=True)

# Data directory.

# data_directory = '/home/RVLuser/data/Exp-LHTCP_Gazebo-20230821'
# data_directory = '/home/RVLuser/data/Exp-LHTCP_K07-20240430/Exp-K07-20240430-4'
data_directory = '/home/RVLuser/data/Exp-LHTCP_K07-20240506/Exp-LHTCP_K07-20240430-4'

# Configuration.

num_images = 10
show_images = False
use_gt = False

# Camera-robot configuration files.

camera_params_file_name = os.path.join(data_directory, 'camera_config/mono/camera0_params.yaml')
camera1_camera0_pose_file_name = os.path.join(data_directory, 'camera_config/stereo/TC1C0.yaml')
camera_robot_pose_gt_file_name = os.path.join(data_directory, 'T_C0_B_gt.npy')
camera_robot_pose_est_file_name = os.path.join(data_directory, 'results/T_C0_B_est.npy')
lhtcp_flange_pose_est_file_name = os.path.join(data_directory, 'results/p_T_E_est.npy')

# Camera intrinsics.

fs=cv2.FileStorage(camera_params_file_name, cv2.FILE_STORAGE_READ)
K = np.asarray(fs.getNode("camera_matrix").mat())

# Pose of the camera1 with respect to the camera0.

fs=cv2.FileStorage(camera1_camera0_pose_file_name, cv2.FILE_STORAGE_READ)
TC1C0 = np.asarray(fs.getNode("TC1C0").mat())

# Camera pose with respect to the robot base.

# K0-7 init setup
TC0B = np.eye(4)
TC0B[:3, :3] = np.array([[-1, 0, 0], 
                         [0, 0, -1],
                         [0, -1, 0]])
TC0B[:3, 3] = np.array([0.52, -1.9, 2.23])
# TC0B[:3, 3] = np.array([0.2, -1., 2.023])
np.save(camera_robot_pose_gt_file_name, TC0B)
# rotz = rvl.rotz(np.deg2rad(90))
# TC0B = TC0B @ rotz

pTE_gt = np.array([-0.6, 0., 1.6])

if use_gt:
    TC0B = np.load(camera_robot_pose_gt_file_name)
    # fs=cv2.FileStorage(camera_robot_pose_gt_file_name, cv2.FILE_STORAGE_READ)
    # TC0B = np.asarray(fs.getNode("TC0B").mat())
    # np.save(os.path.join(data_directory, 'camera_config/TC0B.npy'), TC0B)

# Load data.

TEB = np.zeros((num_images, 4, 4))
TEB[:,3,3] = 1.0
pTC_ = np.zeros((num_images, 4, 1))
pTC_[:,3,0] = 1.0
pTC_ip = np.zeros((num_images, 4, 1))
pTC_ip[:,3,0] = 1.0


for image_idx in range(num_images):
    for camera_idx in range(2):

        if use_gt:
            # Camera pose with respect to the robot base.

            TCB = TC0B.copy()
            if camera_idx == 1:
                TCB = TCB @ TC1C0

        # File names.

        image_file_name = 'results/images/%d-%d.png' % (image_idx, camera_idx)
        image_file_path = os.path.join(data_directory, image_file_name)

        # gt
        lhtcp_gt_file_name = 'poses_achieved/%d_lhtcp.yaml' % image_idx
        lhtcp_gt_file_path = os.path.join(data_directory, lhtcp_gt_file_name)

        lhtcp_ip_file_name = 'results/lhtcp_pose_est/T_M_C0_%d.npy' % image_idx
        lhtcp_ip_file_path = os.path.join(data_directory, lhtcp_ip_file_name)

        # gt
        lhtcp_wrt_camera_file_name = 'LH_pose_in_camera/TLC0_%d.yaml' % image_idx
        lhtcp_wrt_camera_file_path = os.path.join(data_directory, lhtcp_wrt_camera_file_name)
        
        flange_wrt_base_file_name = 'results/flange_poses/T_E_B_%d.npy' % image_idx
        flange_wrt_base_file_path = os.path.join(data_directory, flange_wrt_base_file_name)

        # Flange with respect to the robot base.

        # with open(flange_wrt_base_file_path) as file:
        #     data = yaml.load(file, Loader=SafeLoader)
        # REB = np.reshape(np.array(data['R']), (3,3))
        # tEB = rvl.homogeneous(np.array(data['t'])[np.newaxis,:])        
        # TEB[image_idx,:3,:3] = REB
        # TEB[image_idx,:,3] = tEB
        T_E_B_ = np.load(flange_wrt_base_file_path)
        TEB[image_idx, :, :] = T_E_B_

        if use_gt:
            # LHTCP with respect to the robot base (ground truth).

            with open(lhtcp_gt_file_path) as file:
                data = yaml.load(file, Loader=SafeLoader)
            pTB = rvl.homogeneous(np.array(data['t'])[np.newaxis,:])

            # LHTCP with respect to the flange.

            pTE = rvl.inv_transf(TEB[image_idx,:,:]) @ pTB.T  

            # LHTCP with respect to the camera computed from the coordinates with respect to the robot base (ground truth).

            pTC = pTB @ rvl.inv_transf(TCB).T

            # LHTCP with respect to the camera (ground truth).

            with open(lhtcp_wrt_camera_file_path) as file:
                data = yaml.load(file, Loader=SafeLoader)
            pTC_[image_idx,:3,0] = np.array(data['t'])

        # LHTCP with respect to the camera estimated by image processing.

        # with open(lhtcp_ip_file_path) as file:
        #     data = yaml.load(file, Loader=SafeLoader)
        # tTC_ip = np.array(data['t'])
        # RTC_ip = np.reshape(np.array(data['R']), (3,3))
        # pTC_ip[image_idx,:3,0] = tTC_ip - 0.05 * RTC_ip[:3,2]
        T_M_C_ip = np.load(lhtcp_ip_file_path)
        pTC_ip[image_idx,:3,0] = T_M_C_ip[:3, 3] - 0.05 * T_M_C_ip[:3, 2]

        # Visualization.

        if show_images:
            
            if use_gt:
                # Image projection of the GT LHTCP. 

                lhtcp_img = K @ pTC[0,:3]
                lhtcp_img /= lhtcp_img[2]
                lhtcp_img = np.ceil(lhtcp_img[:2]).astype(int)

            # Image projection of the LHTCP estimated by image processing. 

            if camera_idx == 1:
                pTC_ip_ = rvl.inv_transf(TC1C0) @ pTC_ip[image_idx,:,:]
            else:
                pTC_ip_ = pTC_ip[image_idx,:,:].copy()
            lhtcp_ip_img = K @ pTC_ip_[:3,0]
            lhtcp_ip_img /= lhtcp_ip_img[2]
            lhtcp_ip_img = np.ceil(lhtcp_ip_img[:2]).astype(int)

            if use_gt:
                # Image projection of the GT LHTCP with respect to the camera. 

                if camera_idx == 1:
                    pTC__ = rvl.inv_transf(TC1C0) @ pTC_[image_idx,:,:]
                else:
                    pTC__ = pTC_[image_idx,:,:].copy()
                lhtcp_img_ = K @ pTC__[:3,0]
                lhtcp_img_ /= lhtcp_img_[2]
                lhtcp_img_ = np.ceil(lhtcp_img_[:2]).astype(int)

            # Load image.

            image = cv2.imread(image_file_path)

            # Display LHTCP.

            if use_gt:
                e = np.linalg.norm(pTC[0,:] - pTC__[:,0])
            print('image %d' % image_idx)
            cv2.drawMarker(image, (lhtcp_ip_img[0], lhtcp_ip_img[1]), (0, 0, 255), cv2.MARKER_CROSS)
            if use_gt:
                cv2.drawMarker(image, (lhtcp_img[0], lhtcp_img[1]), (0, 255, 0), cv2.MARKER_CROSS)
                cv2.drawMarker(image, (lhtcp_img_[0], lhtcp_img_[1]), (255, 0, 0), cv2.MARKER_CROSS)
            cv2.imshow('image', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

# Calibration.

RCB = np.eye(4)
RCB[:3,:3] = TC0B[:3,:3]
pert_angle_deg = 45.0
R_pert = rvl.perturbation_rot(pert_angle_deg)
RCB_est_init = R_pert @ RCB
TCB_est, pTE_est = robcamcal.calibrate(TEB, pTC_ip, RCB_est_init)

print(TCB_est - TC0B)
print(pTE_est - pTE_gt)
np.save(camera_robot_pose_est_file_name, TCB_est)
np.save(lhtcp_flange_pose_est_file_name, pTE_est)


T_B_B = TC0B @ TCB_est
RBB = T_B_B[:3,:3]
theta = np.arccos((np.trace(RBB)-1)/2.)
print(np.rad2deg(theta))

# Error.
if use_gt:
    ePos = np.linalg.norm(TCB_est[:,3] - TC0B[:,3])
    eOrient = np.rad2deg(rvl.rot_angle(TCB_est[:3,:3].T @ TC0B[:3,:3]))
    eLHTCP = np.linalg.norm(pTE_est - pTE[:3,0])
    print("Position error = %f Orientation error = %f LTHCP error = %f" % (ePos, eOrient, eLHTCP))


