import numpy as np
import rvlpyutil as rvl

def calibrate(TEB, pTC, RCB_est_init):
    RCB_est = RCB_est_init.copy()
    for iteration in range(3):
        pTD_est = RCB_est @ pTC
        e = pTD_est - np.expand_dims(TEB[:,:,3],2)
        a = np.zeros((TEB.shape[0], 3, 9))
        a[:,:,:3] = TEB[:,:3,:3]
        a[:,:,3:6] = rvl.skew(pTD_est[:,:3,0])
        a[:,:,6:] = -np.eye(3)
        Q = np.sum(np.transpose(a, (0, 2, 1)) @ a, axis=0)
        b = np.sum(np.transpose(a, (0, 2, 1)) @ e[:,:3,:], axis=0)
        x = np.linalg.solve(Q,b)
        pTE_est = x[:3,0]
        q = x[3:6,0]
        tCB_est = x[6:,0]
        th = np.linalg.norm(q)
        if np.abs(th) >= 1e-10:
            u = q / th
            dR = rvl.angle_axis_to_rotmx(u, th)
        else:
            dR = np.eye(4)
        RCB_est = dR @ RCB_est
        u, th = rvl.rotmx_to_angle_axis(RCB_est)
        RCB_est = rvl.angle_axis_to_rotmx(u, th)
    TCB_est = RCB_est.copy()
    TCB_est[:3,3] = tCB_est

    return TCB_est, pTE_est