import numpy as np
import rvlpyutil as rvl
import robcamcal 

# Reference frames

#   E - robot tool
#   B - robot base
#   C - camera

# Camera pose with respect to the robot base.

TCB = np.array([[0.0, 0.0, 1.0, 2.0], [-1.0, 0.0, 0.0, 1.0], [0.0, -1.0, 0.0, 2.0], [0.0, 0.0, 0.0, 1.0]])

# LHTCP position with respect to the robot tool.

pTE = np.array([0.0, 0.5, 1.5, 1.0])

# Samples.

REB0 = np.array([[0.0, 0.0, 1.0, 0.0], [1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
TEB = np.zeros((5, 4, 4))
TEB[0,:,:] = REB0
TEB[0,:3,3] = np.array([0.6, 1.0, 1.5])
TEB[1,:,:] = REB0
TEB[1,:3,3] = np.array([1.0, 1.0, 1.5])
TEB[2,:,:] = REB0
TEB[2,:3,3] = np.array([1.0, 0.6, 1.5])
TEB[3,:,:] = rvl.rotz(np.pi/12.0) @ REB0
TEB[3,:3,3] = np.array([0.8, 1.0, 1.5])
TEB[4,:,:] = rvl.roty(np.pi/12.0) @ REB0
TEB[4,:3,3] = np.array([0.8, 1.0, 1.5])

# LHTCP positions with respect to the robot base

pTB = TEB @ np.expand_dims(pTE, 1)

# LHTCP positions with respect to the camera obtained by image processing.

pTC = np.linalg.inv(TCB) @ pTB

# Initial estimation of RCB.

RCB = np.eye(4)
RCB[:3,:3] = TCB[:3,:3]
pert_angle_deg = 45.0
R_pert = rvl.perturbation_rot(pert_angle_deg)
RCB_est_init = R_pert @ RCB

# Calibration.

TCB_est, pTE_est = robcamcal.calibrate(TEB, pTC, RCB_est_init)

print('complete.')







