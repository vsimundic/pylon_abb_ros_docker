import numpy as np
import rvlpyutil as rvl

def deformation_simulation(y_angle: float, z_angle: float, distance: float) -> np.ndarray:

    beta = y_angle
    gamma = z_angle
    a = distance

    T_D_E_init = np.eye(4)
    T_D_E_init[0, 3] = -a*np.sin(beta)
    T_D_E_init[2, 3] = a*(1 - np.cos(beta))
    
    T_D_E = T_D_E_init @ rvl.roty(beta) @ rvl.rotz(gamma)
    
    return T_D_E