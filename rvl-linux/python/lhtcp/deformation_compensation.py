import numpy as np
import rvlpyutil as rvl

def deformation_compensation(T_C0_B: np.ndarray, p_T_B_goal: np.ndarray, T_E_B: np.ndarray, T_T_C: np.ndarray) -> np.ndarray:
    """
    Perform deformation compensation of the LHTCP. From document: Deformation Simulation and Compensation - R. Cupec
 
    Parameters
    ----------
    T_C0_B : ndarray
        The pose of the referent camera w.r.t. robot base.
    p_T_B_goal : ndarray
        The position of the LHTCP in the goal position w.r.t. robot base.
    T_E_B : ndarray
        The pose of the flange when capturing the image. 
    T_T_C : ndarray
        The pose of the LHTCP in the image. 
 
    Returns
    -------
    ndarray
        The pose of the flange in goal position w.r.t. base.
    """

    p_T_C_ = np.hstack((T_T_C[:3, 3], 1.))
    v_C = T_T_C[:3, 2].copy()

    p_T_E_ = rvl.inv_transf(T_E_B) @ T_C0_B @ p_T_C_ 
    # T_B_E = rvl.inv_transf(T_E_B)
    # t_B_E_goal = T_B_E[:3, 3]
    # p_T_E_ = T_B_E[:3, :3] @ p_T_B_goal + t_B_E_goal

    v_E = T_E_B[:3,:3].T @ T_C0_B[:3, :3] @ v_C 
    R_A_E_goal = np.eye(3)
    R_A_E_goal[:3, 2] = -v_E # Step 1

    # if np.isclose(np.abs(v_E[0]), 1):
    #     R_A_E_goal[:3, 0] = np.array([0, 1, 0]) # Step 2
    # else:
    #     R_A_E_goal[:3, 0] = np.array([1, 0, 0]) # Step 2

    axis_idx = np.argmin(v_E)
    u = np.zeros(3)
    u[axis_idx] = 1.0
    R_A_E_goal[:3,0] = np.cross(u, R_A_E_goal[:3,2])
    R_A_E_goal[:3,0] = R_A_E_goal[:3,0] / np.linalg.norm(R_A_E_goal[:3,0])
    R_A_E_goal[:3, 1] = np.cross(R_A_E_goal[:3, 2], R_A_E_goal[:3, 0]) # Step 3

    p_T_A_goal = R_A_E_goal.T @ p_T_E_[:3] # Step 4

    lambda_c = p_T_A_goal[0] * p_T_B_goal[0] + p_T_A_goal[1] * p_T_B_goal[1]
    lambda_s = p_T_A_goal[1] * p_T_B_goal[0] - p_T_A_goal[0] * p_T_B_goal[1]
    lambda_0 = p_T_A_goal[2] * p_T_B_goal[2]
    alpha = np.arctan2(lambda_s, lambda_c) # Step 5

    R_B_A = rvl.rotz(alpha)[:3, :3] # Step 6
    R_B_E_goal = R_A_E_goal @ R_B_A # Step 7
    t_B_E_goal = p_T_E_[:3] - R_B_E_goal @ p_T_B_goal # step 8
    T_EG_B = np.eye(4) # Step 9
    T_EG_B[:3, :3] = R_B_E_goal.T
    T_EG_B[:3, 3] = -R_B_E_goal.T @ t_B_E_goal 

    return T_EG_B.copy()


