import numpy as np
from deformation_simulation import deformation_simulation
from deformation_compensation import deformation_compensation

p_T_B_goal = np.load('data/p_T_B_goal.npy')
T_C0_B = np.load('data/T_C0_B.npy')
T_E_B_deformation = np.load('data/T_E_B_deformation.npy')
T_E_B_detection = np.load('data/T_E_B_detection.npy')
T_E_B_goal_gt = np.load('data/T_E_B_goal_gt.npy')
T_T_C0 = np.load('data/T_T_C0.npy')
T_T_E = np.load('data/T_T_E.npy')

T_ED_E = deformation_simulation(np.deg2rad(0.260966797145819), np.deg2rad(-12.3461948099133), 1.7)
T_ED_B_detection = T_E_B_detection @ T_ED_E
# T_ED_E = np.linalg.inv(T_E_B_detection) @ T_E_B_deformation

T_EG_B = deformation_compensation(T_C0_B, p_T_B_goal, T_E_B_detection, T_T_C0)

T_EDG_B = T_EG_B @ T_ED_E
T_T_B = T_EDG_B @ T_T_E 
print(T_T_B)
print('completed.')