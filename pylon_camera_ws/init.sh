rm -rf /home/RVLuser/pylon_camera_ws/build
rm -rf /home/RVLuser/pylon_camera_ws/devel

sh -c 'echo "yaml https://raw.githubusercontent.com/basler/pylon-ros-camera/master/pylon_camera/rosdep/pylon_sdk.yaml" > /etc/ros/rosdep/sources.list.d/30-pylon_camera.list'
rosdep update 
rosdep install --from-paths . --ignore-src --rosdistro=$ROS_DISTRO -y

cd /home/RVLuser/pylon_camera_ws
catkin_make clean
catkin_make