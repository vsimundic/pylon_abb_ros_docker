#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2
import sys
import numpy as np
import message_filters
import os
import yaml
from rospkg import RosPack
import moveit_commander
import matplotlib.pyplot as plt
from tf.transformations import quaternion_matrix
from trac_ik_python.trac_ik import IK
from tf.transformations import quaternion_from_matrix, quaternion_matrix, euler_from_matrix
from geometry_msgs.msg import Pose, PoseStamped
import rvlpyutil as rvl
from lhtcp_detector import LHTCPDetector
import traceback
import readline
from gazebo_msgs.srv import SpawnModel, SetModelConfiguration, SetModelConfigurationRequest, DeleteModel, GetModelState, GetJointProperties, GetJointPropertiesRequest
from get_transformation import get_transformation
import csv
import signal
from subprocess import check_output
import roslaunch
import subprocess
from rospy.exceptions import ROSTimeMovedBackwardsException
import time

sys.path.append('/home/RVLuser/rvl-linux/python')
from lhtcp.deformation_simulation import deformation_simulation
from lhtcp.deformation_compensation import deformation_compensation


def get_pid(name: str):
    return list(map(int, check_output(['pidof', name]).split()))

def kill_gazebo_processes():
    try:
        gzserver_pids = get_pid('gzserver')
        if len(gzserver_pids) > 0:
            for pid in gzserver_pids:
                os.kill(pid, signal.SIGKILL)
    except Exception as e:
        pass
    try:
        gzclient_pids = get_pid('gzclient')
        if len(gzclient_pids) > 0:
            for pid in gzclient_pids:
                os.kill(pid, signal.SIGKILL)
    except Exception as e:
        pass
    try:
        rviz_pids = get_pid('rviz')
        if len(rviz_pids) > 0:
            for pid in rviz_pids:
                os.kill(pid, signal.SIGKILL)
    except Exception as e:
        pass
    try:
        rviz_pids = get_pid('move_group')
        if len(rviz_pids) > 0:
            for pid in rviz_pids:
                os.kill(pid, signal.SIGKILL)
    except Exception as e:
        pass
    try:
        rviz_pids = get_pid('robot_state_pub')
        if len(rviz_pids) > 0:
            for pid in rviz_pids:
                os.kill(pid, signal.SIGKILL)
    except Exception as e:
        pass

class LHTCPDetectorCompensator:
    def __init__(self, config) -> None:
        np.set_printoptions(precision=20, linewidth=100)
        np.set_printoptions(suppress=True)
        self.config = config

        self.lhtcp_detector = LHTCPDetector(self.config['LHTCP_detector_config_path'],
                                            self.config['cameras_params_dir'],
                                            self.config['extrinsic_camera_params_dir'])
        
        self.camera0_topic = self.config['camera0_topic']
        self.camera1_topic = self.config['camera1_topic']
        
        self.root_path = self.config['save_path']
        self.save_path = os.path.join(self.config['save_path'], 'results')
        self.save_images_path = os.path.join(self.save_path, 'images')
        if not os.path.exists(self.save_images_path):
            os.makedirs(self.save_images_path)
        
        # Image process setup
        self.bridge = CvBridge()
        self.image_sub_cam0 = message_filters.Subscriber(self.config['camera0_topic'], Image)
        self.image_sub_cam1 = message_filters.Subscriber(self.config['camera1_topic'], Image)
        self.ts = message_filters.ApproximateTimeSynchronizer([self.image_sub_cam0, self.image_sub_cam1], 100, 10, allow_headerless=True)

        # Robot setup
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group_name = self.config['robot_group_name']
        self.group = moveit_commander.MoveGroupCommander(self.group_name)
        self.ik = IK('base_link', 'tool0', timeout=0.05, solve_type='Distance')
        
        self.add_scene()

        self.T_EG_B_gt = np.eye(4)
        if os.path.exists(os.path.join(self.root_path, 'T_EG_B_gt.npy')):
           self.T_EG_B_gt = np.load(os.path.join(self.root_path, 'T_EG_B_gt.npy'))
           
        self.T_E_B = np.eye(4)
        self.T_D_E = np.eye(4)

        # LHTCP tip to flange
        self.T_T_E = np.array([[0, 0, 1, -0.5],
                               [0, -1, 0, 0],
                               [1, 0, 0, 1.7+0.001],
                               [0, 0, 0, 1]])
        
        # Robot-camera calibration
        self.T_C0_B = np.load(os.path.join(self.root_path, 'camera_config', 'T_C0_B.npy'))

        # Camera 1 in camera 0 frame
        self.T_C1_C0 = np.load(os.path.join(self.root_path, 'camera_config', 'stereo', 'T_C1_C0.npy'))

        s = cv2.FileStorage()
        s.open(os.path.join(self.root_path, 'camera_config', 'mono', 'camera0_params.yaml'), cv2.FileStorage_READ)
        self.camera_matrix0 = s.getNode('camera_matrix').mat()
        self.dist_coeffs0 = s.getNode('distortion_coefficients').mat()
        s.release()

        s = cv2.FileStorage()
        s.open(os.path.join(self.root_path, 'camera_config', 'mono', 'camera1_params.yaml'), cv2.FileStorage_READ)
        self.camera_matrix1 = s.getNode('camera_matrix').mat()
        self.dist_coeffs1 = s.getNode('distortion_coefficients').mat()
        s.release()

        # self.T_T_M = rvl.roty(np.pi) 
        self.T_T_M = np.eye(4) 
        self.T_T_M[2, 3] = -0.05
        

    def __help(self):
        print('goal - Set the lhtcp inside the goal hoop and type this command to save the robot pose')
        print('detect - Set the lhtcp between the cameras to detect the lhtcp and get the lhtcp pose w.r.t. reference camera')
        print('deformation <beta> <gamma> - Simulate deformation of the lhtcp. Beta and gamma are deformation angles about y- and z-axis, respectively.')
        print('compensation - Perform deformation compensation to get T_E_B in goal')
        print('exit - Exit this program')
        print('help - print this message')


    def add_scene(self):
        self.scene.clear()
        # Add ground to scene
        is_on_scene = 'ground_box' in self.scene.get_known_object_names()
        if not is_on_scene:
            box_pose = PoseStamped()
            box_pose.header.frame_id = 'base_link'
            box_pose.pose.orientation.w = 1.0
            box_pose.pose.position.z = -0.01
            box_name = 'ground_box'
            self.scene.add_box(box_name, box_pose, size=(6, 6, 0.01))


    def run(self):
        print("Running LHTCP detection and compensation program.")
        print("Type \"help\" to see the commands.")

        beta = 0
        gamma = 0
        a = 0
        T_M_C0 = np.eye(4)
        T_T_C0 = np.eye(4)
        while True:
            try:
                text = input(">>")
                input_split = text.split(' ')
                command = input_split[0]

                if command == 'help':
                    self.__help()

                elif command == 'goal':
                    if len(input_split) == 2:
                        if input_split[-1] == 'set':

                            T_hoop_B = get_transformation('goal_hoop_link', 'base_link')

                            T_E_B_current_goal = self.get_current_tool_pose()
                            T_T_B_current_goal = T_E_B_current_goal @ self.T_T_E
                            T_T_B_real_goal = T_T_B_current_goal.copy()
                            T_T_B_real_goal[:3, 3] = T_hoop_B[:3, 3].copy()
                            T_T_B_real_goal[2, 3] -= 0.1

                            T_E_B_goal = T_T_B_real_goal @ rvl.inv_transf(self.T_T_E)

                            joints_current = self.get_current_joint_values()
                            joints_goal = None
                            for i in range(50):
                                joints_goal = self.get_inverse_kin(joints_current, T_E_B_goal)
                                print(i)
                                if joints_goal is not None:
                                    print('Found kin.')
                                    self.send_joint_values_to_robot(joints_goal, wait=True)
                                    break
                            if joints_goal is None:
                                print('Cannot find inverse kin.')
                                continue

                        if input_split[-1] == 'save':
                            self.T_EG_B_gt = self.get_current_tool_pose()
                            joints_goal_gt = self.get_current_joint_values()
                            np.save(os.path.join(self.root_path, 'T_EG_B_gt.npy'), self.T_EG_B_gt)
                            np.save(os.path.join(self.root_path, 'joints_goal.npy'), np.array(joints_goal_gt))
                            print('Goal pose saved.')
                        elif input_split[-1] == 'go':
                            joints_goal_gt = np.load(os.path.join(self.root_path, 'joints_goal.npy')).tolist()
                            self.send_joint_values_to_robot(joints_goal_gt)

                elif command == 'setup':
                    # joints = self.get_current_joint_values()
                    # np.save(os.path.join(self.root_path, 'joints_detection.npy'), np.array(joints))
                    if len(input_split) == 2:
                        if input_split[-1] == 'save':
                            joints_detection = self.get_current_joint_values()
                            np.save(os.path.join(self.root_path, 'joints_detection.npy'), np.array(joints_detection))
                            print('Setup for detection pose saved.')
                        elif input_split[-1] == 'go':
                            self.__setup_go()
                            # joints_detection = np.load(os.path.join(self.root_path, 'joints_detection.npy')).tolist()
                            # self.send_joint_values_to_robot(joints_detection)

                elif command == 'deformation':
                    if len(input_split) == 3:
                        try:
                            beta = np.deg2rad(float(input_split[-2]))
                            gamma = np.deg2rad(float(input_split[-1]))
                            a = self.T_T_E[2, 3]
                        except ValueError as e:
                            print('ValueError: ' + e)
                    else:
                        print('Wrong use of the command deformation.')
                        print(help)
                        continue

                elif command == 'detect':
                    T_M_C0 = self.__detect(beta, gamma, a, visualize=True)
                    self.T_T_C0 = T_M_C0 @ self.T_T_M
                    # self.T_E_B = self.get_current_tool_pose()
                    # self.T_D_E = deformation_simulation(beta, gamma, a)
                    # self.T_E_B_def = self.T_E_B @ self.T_D_E

                    # joints_current = self.get_current_joint_values()
                    # joints_goal = None
                    # for i in range(50):
                    #     joints_goal = self.get_inverse_kin(joints_current, self.T_E_B_def)
                    #     print(i)
                    #     if joints_goal is not None:
                    #         print('Found kin.')
                    #         self.send_joint_values_to_robot(joints_goal, wait=True)
                    #         break
                    # if joints_goal is None:
                    #     print('Cannot find inverse kin.')
                    #     continue

                    # # self.send_pose_to_robot(self.T_E_B_def)
                    # # T_E_B_ = self.get_current_tool_pose()
                    # rospy.sleep(5)
                    
                    # msg0 = rospy.wait_for_message(self.camera0_topic, Image)
                    # msg1 = rospy.wait_for_message(self.camera1_topic, Image)
                    # image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
                    # image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')
                    # img0_filename = self.save_images_path + '/{}-{}.png'.format('img', 0)
                    # img1_filename = self.save_images_path + '/{}-{}.png'.format('img', 1)
                    # cv2.imwrite(img0_filename, image0)
                    # cv2.imwrite(img1_filename, image1)

                    # T_M_C0, solution0, solution1 = self.lhtcp_detector.detect(img0_filename ,img1_filename)
                    # T_M_C0 = T_M_C0.astype(np.float64)
                    # T_M_C1 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0
                    # # self.lhtcp_detector.visualize_from_pose(T_M_C0, T_M_C1)
                    
                    # np.save(os.path.join(self.root_path, 'T_M_C0_det.npy'), T_M_C0)
                    # np.save(os.path.join(self.root_path, 'T_M_C1_det.npy'), T_M_C1)

                    # T_T_C0 = T_M_C0 @ self.T_T_M

                    # T_T_B_det = self.T_C0_B @ T_T_C0
                    # T_T_B_gt = get_transformation('lhtcp_tip_link', 'base_link')
                    # print('T_T_B_det - T_T_B_gt:')
                    # print(T_T_B_det - T_T_B_gt)

                    # # # point3d = T_T_C0[:3, 3].copy()
                    # # T_S_B = np.eye(4)
                    # # T_S_B[:3,3] = np.array([0.515581, -2.3742, 2.32515])
                    # # T_S_C0 = rvl.inv_transf(self.T_C0_B) @ T_S_B

                    # # point3d = T_S_C0[:3,3]
                    # # point3d_h = np.append(point3d, 1)

                    # T_M_C1 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0
                    # T_M_C1 = self.T_C1_C0 @ T_M_C0

                    # # point3d_h_c1 = rvl.inv_transf(self.T_C1_C0) @ point3d_h
                    # # # point3d_h_c1 = T_M_C1[:3,3].copy()
                    # # # point3d_h_c1 = np.append(point3d_h_c1, 1)
                    # # # point3d_h_c1 = self.T_C1_C0 @ point3d_h
                    
                    # # projected_point_h = np.dot(np.c_[self.camera_matrix0, np.zeros(3)], point3d_h)
                    # # projected_point_2d = projected_point_h[:2] / projected_point_h[2]
                    # # print(projected_point_2d)
                    # # projected_point_2d_int = (int(projected_point_2d[0]), int(projected_point_2d[1]))
                    # # cv2.circle(solution0, projected_point_2d_int, radius=2, color=(0, 255, 0), thickness=2)

                    # # projected_point_h = np.dot(np.c_[self.camera_matrix1, np.zeros(3)], point3d_h_c1)
                    # # projected_point_2d = projected_point_h[:2] / projected_point_h[2]
                    # # print(projected_point_2d)
                    # # projected_point_2d_int = (int(projected_point_2d[0]), int(projected_point_2d[1]))
                    # # cv2.circle(solution1, projected_point_2d_int, radius=2, color=(0, 255, 0), thickness=2)


                    # # T_M_C0_gt = np.load(os.path.join(self.root_path, 'T_M_C0_gt.npy'))
                    # # point3d = T_M_C0_gt[:3,3]
                    # # point3d_h = np.append(point3d, 1)

                    # # # T_M_C1 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0
                    # # # T_M_C1 = self.T_C1_C0 @ T_M_C0

                    # # point3d_h_c1 = rvl.inv_transf(self.T_C1_C0) @ point3d_h
                    # # # point3d_h_c1 = T_M_C1[:3,3].copy()
                    # # # point3d_h_c1 = np.append(point3d_h_c1, 1)
                    # # # point3d_h_c1 = self.T_C1_C0 @ point3d_h
                    
                    # # projected_point_h = np.dot(np.c_[self.camera_matrix0, np.zeros(3)], point3d_h)
                    # # projected_point_2d = projected_point_h[:2] / projected_point_h[2]
                    # # print(projected_point_2d)
                    # # projected_point_2d_int = (int(projected_point_2d[0]), int(projected_point_2d[1]))
                    # # cv2.circle(solution0, projected_point_2d_int, radius=2, color=(0, 255, 0), thickness=2)

                    # # projected_point_h = np.dot(np.c_[self.camera_matrix1, np.zeros(3)], point3d_h_c1)
                    # # projected_point_2d = projected_point_h[:2] / projected_point_h[2]
                    # # print(projected_point_2d)
                    # # projected_point_2d_int = (int(projected_point_2d[0]), int(projected_point_2d[1]))
                    # # cv2.circle(solution1, projected_point_2d_int, radius=2, color=(0, 255, 0), thickness=2)


                    # cv2.imshow('Solution0', solution0)
                    # cv2.imshow('Solution1', solution1)
                    # cv2.waitKey(0)
                    # cv2.destroyAllWindows()

                elif command == 'compensation':
                    self.__compensation(self.T_T_C0)
                    self.T_T_B_gt = self.T_EG_B_gt @ self.T_T_E

                    np.save(os.path.join(self.root_path, 'p_T_B_goal.npy'), self.T_T_B_gt[:3, 3])
                    np.save(os.path.join(self.root_path, 'T_C0_B.npy'), self.T_C0_B)
                    np.save(os.path.join(self.root_path, 'T_E_B_deformation.npy'), self.T_E_B_def)
                    np.save(os.path.join(self.root_path, 'T_E_B_detection.npy'), self.T_E_B)
                    np.save(os.path.join(self.root_path, 'T_E_B_goal_gt.npy'), self.T_EG_B_gt)
                    np.save(os.path.join(self.root_path, 'T_T_C0.npy'), self.T_T_C0)
                    np.save(os.path.join(self.root_path, 'T_T_E.npy'), self.T_T_E)

                    # # T_EG_B = deformation_compensation(self.T_C0_B, self.T_T_B_gt[:3, 3], self.T_E_B, T_T_C0)
                    # T_hoop_B = get_transformation('goal_hoop_link', 'base_link')
                    
                    # T_EG_B = deformation_compensation(self.T_C0_B, T_hoop_B[:3, 3], self.T_E_B, T_T_C0)

                    # T_T_B_res = T_EG_B @ self.T_D_E @ self.T_T_E
                    
                    # print('T_T_B_gt - T_T_B_res:')
                    # print(self.T_T_B_gt - T_T_B_res)



                    # joints_current = self.get_current_joint_values()
                    # joints_goal = None
                    # for i in range(50):
                    #     joints_goal = self.get_inverse_kin(joints_current, T_EG_B @ self.T_D_E)
                    #     print(i)
                    #     if joints_goal is not None:
                    #         print('Found kin.')
                    #         self.send_joint_values_to_robot(joints_goal, wait=True)
                    #         break    
                    
                    # if joints_goal is None:
                    #     print('Cannot compute inverse kin...')
                    #     # np.save(os.path.join(self.root_path, 'T_EG_B_broken.npy'), T_EG_B)
                    #     # np.save(os.path.join(self.root_path, 'T_T_B_broken.npy'), T_T_B_res)
                    # else:
                    #     T_T_hoop = get_transformation('lhtcp_tip_link', 'goal_hoop_link')
                    #     # print(T_T_hoop)

                    # print('T_T_B_res - T_hoop_B:')
                    # print(T_T_B_res - T_hoop_B)
                    

                elif command == 'visualize':
                    # T_Tt_E = self.T_T_E.copy()
                    # T_Tt_E[:3, 3] = np.array([0,0,0.05])
                    # T_Tt_B = np.load(os.path.join(self.root_path, 'T_Tt_B.npy'))
                    # T_Tt_C0 = rvl.inv_transf(self.T_C0_B) @ T_Tt_B

                    # T_Tt_C0 = np.eye(4)
                    # T_Tt_C0 = rvl.roty(np.deg2rad(-105)) @ rvl.rotx(np.deg2rad(90))
                    # T_Tt_C0[:3, 3] = np.array([0.5686294300965953, 0.39366894255345897, -1.2243303513468764])
                    # T_Tt_C0[:3, 3] = np.array([0.5686294300965953, 0.39366894255345897, -1.2243303513468764])
                    # T_Tt_C0 = rvl.inv_transf(self.T_C0_B) @ self.T_E_B_def @ T_Tt_E 

                    # T_M_Tt = rvl.roty(np.pi)
                    # T_M_Tt[:3, 3] = np.array([1.7, 0, 0.5-0.05])
                    # T_M_C0_gt = T_Tt_C0 @ T_M_Tt

                    T_T_M_gt = np.eye(4)
                    # T_T_M_gt = rvl.roty(np.pi)
                    T_T_M_gt[2,3] = -0.05

                    T_T_B = np.load(os.path.join(self.root_path, 'T_T_B.npy'))
                    T_T_C0 = rvl.inv_transf(self.T_C0_B) @ T_T_B
                    T_M_C0_gt = T_T_C0 @ rvl.inv_transf(T_T_M_gt)

                    T_M_C0_gt1 = np.load(os.path.join(self.root_path, 'T_M_C0_gt.npy'))
                    T_M_C1_gt1 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0_gt1
                    

                    # print(T_Tt_C0)
                    T_M_C0_gt2 = rvl.inv_transf(self.T_C0_B) @ self.T_E_B @ self.T_D_E @ self.T_T_E @ rvl.inv_transf(T_T_M_gt)
                    T_M_C1_gt2 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0_gt2
                    # print(T_M_C0_gt)
                    T_M_C1 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0

                    if len(input_split) == 2:
                        if input_split[-1] == 'gt1':
                            self.lhtcp_detector.visualize_from_pose(T_M_C0_gt1, T_M_C1_gt1)
                        if input_split[-1] == 'gt2':
                            self.lhtcp_detector.visualize_from_pose(T_M_C0_gt2, T_M_C1_gt2)

                        elif input_split[-1] == 'detect':
                            self.lhtcp_detector.visualize_from_pose(T_M_C0, T_M_C1)

                elif command == 'sphere':
                    T_S_B = np.eye(4)
                    T_S_B[:3,3] = np.array([0.515581, -2.3742, 2.32515])
                    T_S_C0 = rvl.inv_transf(self.T_C0_B) @ T_S_B
                    self.spawn_model_gazebo_sphere(T_S_B, ref_frame='camera0_optical_link')

                elif command == 'exit':
                    del self.lhtcp_detector.detector
                    rospy.signal_shutdown('User initiated exit.')
                    break

            except Exception as error:
                traceback.print_exc()


    def __setup_go(self):
        joints_detection = np.load(os.path.join(self.root_path, 'joints_detection.npy')).tolist()
        self.send_joint_values_to_robot(joints_detection)


    def __detect(self, beta, gamma, a, visualize=False):
        self.T_E_B = self.get_current_tool_pose()
        self.T_D_E = deformation_simulation(beta, gamma, a)
        self.T_E_B_def = self.T_E_B @ self.T_D_E

        joints_current = self.get_current_joint_values()
        joints_goal = None
        for i in range(50):
            joints_goal = self.get_inverse_kin(joints_current, self.T_E_B_def)
            if joints_goal is not None:
                print('Found kin. - detect')
                self.send_joint_values_to_robot(joints_goal, wait=True)
                break
        if joints_goal is None:
            print('Cannot find inverse kin.')
            return None

        time.sleep(10)
        
        msg0 = rospy.wait_for_message(self.camera0_topic, Image)
        msg1 = rospy.wait_for_message(self.camera1_topic, Image)
        image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
        image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')
        img0_filename = self.save_images_path + '/{}-{}.png'.format('img', 0)
        img1_filename = self.save_images_path + '/{}-{}.png'.format('img', 1)
        cv2.imwrite(img0_filename, image0)
        cv2.imwrite(img1_filename, image1)

        T_M_C0, solution0, solution1 = self.lhtcp_detector.detect(img0_filename ,img1_filename)
        T_M_C0 = T_M_C0.astype(np.float64)
        T_T_C0 = T_M_C0 @ self.T_T_M

        if visualize:
            cv2.imshow('Solution0', solution0)
            cv2.imshow('Solution1', solution1)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        T_T_B_det = self.T_C0_B @ T_T_C0
        T_T_B_gt = get_transformation('lhtcp_tip_link', 'base_link')
        print('T_T_B_det - T_T_B_gt:')
        print(T_T_B_det - T_T_B_gt)

        # T_M_C1 = rvl.inv_transf(self.T_C1_C0) @ T_M_C0
        # self.lhtcp_detector.visualize_from_pose(T_M_C0, T_M_C1)
        
        return T_M_C0


    def __compensation(self, T_T_C0):
        self.T_T_B_gt = self.T_EG_B_gt @ self.T_T_E

        T_hoop_B = get_transformation('goal_hoop_link', 'base_link')
        T_hoop_B[2, 3] -= 0.1
        T_EG_B = deformation_compensation(self.T_C0_B, T_hoop_B[:3, 3], self.T_E_B, T_T_C0)

        # T_T_B_res = T_EG_B @ self.T_D_E @ self.T_T_E
        T_EG_B_corrected = T_EG_B @ self.T_D_E

        # T_EG_B_corrected_prev = T_EG_B_corrected.copy()
        # T_EG_B_corrected_prev[2, 3] -= 0.1

        # joints_current = self.get_current_joint_values()
        # joints_goal = None
        # for i in range(100):
        #     joints_goal = self.get_inverse_kin(joints_current, T_EG_B_corrected_prev)
        #     if joints_goal is not None:
        #         print('Found kin.')
        #         self.send_joint_values_to_robot(joints_goal, wait=True)
        #         break    

        T_T_B_res = T_EG_B @ self.T_D_E @ self.T_T_E
        
        print('T_T_B_gt - T_T_B_res:')
        print(self.T_T_B_gt - T_T_B_res)

        joints_current = self.get_current_joint_values()
        joints_goal = None
        for i in range(100):
            joints_goal = self.get_inverse_kin(joints_current, T_EG_B_corrected)
            # print(i)
            if joints_goal is not None:
                print('Found kin. - goal')
                self.send_joint_values_to_robot(joints_goal, wait=True)
                break    
        
        time.sleep(10)


        T_E_B_current = self.get_current_tool_pose()
        print('Pose difference:')
        print(T_EG_B_corrected - T_E_B_current)

        if joints_goal is None:
            print('Cannot compute inverse kin...')
            return None
        else:
            T_T_hoop = get_transformation('lhtcp_tip_link', 'goal_hoop_link')
            return T_T_hoop[:3, 3]
        

    def run_experiments(self, min_angles, max_angles, n):
        np.random.seed(100)


        with open(os.path.join(self.root_path, 'sim_experiments.csv'), 'w') as f:

            writer = csv.writer(f)

            writer.writerow(['beta', 'gamma', 'xdif', 'ydif', 'zdif'])

            for i in range(n):
                print('i = %d' % i)
                beta_deg = np.random.uniform(min_angles, max_angles)
                gamma_deg = np.random.uniform(min_angles, max_angles)
                print('Beta: %.2f, Gamma: %.2f' % (beta_deg, gamma_deg))

                self.__setup_go()
                rospy.sleep(5)
                T_M_C0 = self.__detect(np.deg2rad(beta_deg), np.deg2rad(gamma_deg), a=self.T_T_E[2, 3])
                
                dif = None
                if T_M_C0 is not None:
                    T_T_C0 = T_M_C0 @ self.T_T_M
                    dif = self.__compensation(T_T_C0)
                    if dif is not None:
                        print('Dif: ' + np.array2string(dif, precision=3, separator=',', suppress_small=True))
                    else:
                        print('Dif: None')
                    rospy.sleep(5)
                
                if dif is None:
                    writer.writerow([beta_deg, gamma_deg, None, None, None])
                else:
                    writer.writerow([beta_deg, gamma_deg, dif[0], dif[1], dif[2]])
                    print([beta_deg, gamma_deg, dif[0], dif[1], dif[2]])

                rospy.sleep(5)

    
        del self.lhtcp_detector.detector
        rospy.signal_shutdown('User initiated exit.')



    def run_experiment(self, beta_deg, gamma_deg):

        print('Beta: %.2f, Gamma: %.2f' % (beta_deg, gamma_deg))

        self.__setup_go()
        time.sleep(5)
        T_M_C0 = self.__detect(np.deg2rad(beta_deg), np.deg2rad(gamma_deg), a=self.T_T_E[2, 3])
        
        dif = None
        if T_M_C0 is not None:
            T_T_C0 = T_M_C0 @ self.T_T_M
            dif = self.__compensation(T_T_C0)
            if dif is not None:
                print('Dif: ' + np.array2string(dif, precision=3, separator=',', suppress_small=True))
            else:
                print('Dif: None')
            time.sleep(5)
        
        return dif


    def get_current_tool_pose(self):
        pose_ = self.group.get_current_pose()

        return self.__pose_stamped_to_matrix(pose_)

    def get_current_joint_values(self) -> list:
        return list(self.group.get_current_joint_values())

    def get_inverse_kin(self, q_init: list, T: np.ndarray) -> list:

        pose_ = self.__matrix_to_pose(T)
        
        q = self.ik.get_ik(q_init, pose_.position.x, 
                       pose_.position.y, 
                       pose_.position.z,
                       pose_.orientation.x, 
                       pose_.orientation.y, 
                       pose_.orientation.z, 
                       pose_.orientation.w)
        if q is None:
            return None
        return list(q)

    def send_joint_values_to_robot(self, joint_values: list,  wait: bool=True):
        self.group.set_joint_value_target(joint_values)
        self.group.go(wait=wait)
        self.group.stop()

    def send_pose_to_robot(self, T: np.ndarray, wait: bool=True, cartesian: bool=False):

        pose_goal = self.__matrix_to_pose(T)

        if cartesian:
            plan, _ = self.group.compute_cartesian_path([pose_goal], 0.01, 0.0)
            self.group.execute(plan, wait=wait)
            self.group.clear_pose_targets()
            return

        self.group.set_pose_target(pose_goal)
        success = self.group.go(wait=wait)
        self.group.stop()
        self.group.clear_pose_targets()
        return success
    
    def send_multiple_poses_to_robot(self, Ts: list, wait: bool=True, cartesian: bool=False, at_once: bool=True):
        
        poses_ = [self.__matrix_to_pose(T_) for T_ in Ts]

        if at_once:
            if cartesian:
                    plan, _ = self.group.compute_cartesian_path(poses_, 0.01, 0.0)
                    self.group.execute(plan, wait=wait)
            else:
                    self.group.set_pose_targets(poses_)
                    plan = self.group.plan()
                    self.group.execute(plan[1], wait=wait)
                    # plan = self.group.go(wait=wait)
        else:
            len_poses = len(poses_) - 1
            for idx, pose_goal in enumerate(poses_):
                rospy.loginfo('Pose %d/%d' % (idx, poses_))
                # pose_goal = self.__matrix_to_pose(T)
                if cartesian:
                    plan, _ = self.group.compute_cartesian_path([pose_goal], 0.01, 0.0)
                    self.group.execute(plan, wait=wait)
                else:
                    self.group.set_pose_target(pose_goal)
                    plan = self.group.go(wait=wait)
                
                self.group.stop()
                self.group.clear_pose_targets()


    def pose_stamped_to_matrix(self, pose: PoseStamped) -> np.ndarray: 
        pose_ = pose.pose
        r_ = pose_.orientation
        t_ = pose_.position
        q_ = [r_.x, r_.y, r_.z, r_.w]
        
        T = quaternion_matrix(q_)
        T[:3, 3] = np.array([t_.x, t_.y, t_.z])


    # TEST METHOD
    def spawn_model_gazebo_sphere(self, T_S_B, ref_frame):

        # TEST SPHERE
        with open('/home/RVLuser/pylon_camera_ws/sphere.urdf', 'r') as f:
            urdf = f.read()

        model_name = 'sphere'
        pose_ = self.__matrix_to_pose(T_S_B)
        reference_frame = ref_frame

        rospy.wait_for_service('/gazebo/spawn_urdf_model')
        try:
            spawn_urdf_model_proxy = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
            # _ = spawn_urdf_model_proxy(spawn_model_msg)
            _ = spawn_urdf_model_proxy(model_name, urdf, '/', pose_, '')
            rospy.loginfo('URDF model spawned successfully')
        except rospy.ServiceException as e:
            rospy.logerr('Failed to spawn URDF model: %s' % e)

            
    @staticmethod
    def __pose_stamped_to_matrix(pose: PoseStamped):
        pose_ = pose.pose
        q_ = [pose_.orientation.x, pose_.orientation.y, pose_.orientation.z, pose_.orientation.w]
        t_ = np.array([pose_.position.x, pose_.position.y, pose_.position.z])
        T = quaternion_matrix(q_)
        T[:3, 3] = np.array(t_)
        return T

    @staticmethod
    def __matrix_to_pose(T: np.ndarray):
        q = quaternion_from_matrix(T) # xyzw

        pose = Pose()
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]
        pose.position.x = T[0, 3]
        pose.position.y = T[1, 3]
        pose.position.z = T[2, 3]
        return pose

def run_bash_command(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
    return process

def run_experiments_lhtcp(config):
    np.random.seed(10)
    min_angles = -15.0
    max_angles = 15.0
    n = 5
    
    script_path = '/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619/start_gazebo.bash'

    with open(os.path.join('/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619', 'sim_experiments.csv'), 'w') as f:

        writer = csv.writer(f)

        writer.writerow(['beta', 'gamma', 'xdif', 'ydif', 'zdif'])

        for i in range(n):
            
            kill_gazebo_processes()
            process = run_bash_command(f"bash {script_path}")

            time.sleep(10.0)

            lhtcp_detector_compensator = LHTCPDetectorCompensator(config)

            beta_deg = np.random.uniform(min_angles, max_angles)
            gamma_deg = np.random.uniform(min_angles, max_angles)

            dif = lhtcp_detector_compensator.run_experiment(beta_deg, gamma_deg)

            if dif is None:
                writer.writerow([beta_deg, gamma_deg, None, None, None])
            else:
                writer.writerow([beta_deg, gamma_deg, dif[0], dif[1], dif[2]])
                print([beta_deg, gamma_deg, dif[0], dif[1], dif[2]])

            time.sleep(5)

            os.killpg(os.getpgid(process.pid), signal.SIGTERM)
            output, error = process.communicate()
            print("Output:\n", output.decode())
            print("Error:\n", error.decode())
            print("Process terminated.")

    
    rospy.signal_shutdown('User initiated exit.')


def main(args):

    moveit_commander.roscpp_initialize(args)
    rospy.init_node('lhtcp_detection_compensation', anonymous=True)
    
    rp = RosPack()
    package_path = rp.get_path('lhtcp_detect')
    config_path = os.path.join(package_path, 'config')

    with open(os.path.join(config_path, 'config.yaml'), 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    # run_experiments_lhtcp(config)
    
    lhtcp_detector_compensator = LHTCPDetectorCompensator(config)

    
    min_angles = -15.0
    max_angles = 15.0
    n = 50
    # lhtcp_detector_compensator.run_experiments(min_angles, max_angles, n)
    lhtcp_detector_compensator.run()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

