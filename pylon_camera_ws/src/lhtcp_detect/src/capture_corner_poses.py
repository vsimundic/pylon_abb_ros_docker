#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import sys
import numpy as np
import message_filters
import os
import yaml
from rospkg import RosPack
import moveit_commander
import moveit_msgs.msg
from geometry_msgs.msg import PoseStamped
from glob import glob
from shapely.geometry import Polygon, Point
import matplotlib.pyplot as plt
from tf.transformations import quaternion_matrix
import rvlpyutil as rvl

np.random.seed(50)
number_of_points = 1000
rot_max_deg = 20
rot_min_deg = -20



class ImagePoseSaver:
    def __init__(self, config):
        self.__config = config

        self.load_data = bool(self.__config['load_data'])

        self.bridge = CvBridge()

        self.image_sub_cam0 = message_filters.Subscriber(self.__config['camera0_topic'], Image)
        self.image_sub_cam1 = message_filters.Subscriber(self.__config['camera1_topic'], Image)

        self.ts = message_filters.ApproximateTimeSynchronizer([self.image_sub_cam0, self.image_sub_cam1], 100, 10, allow_headerless=True)
        self.ts.registerCallback(self.callback)

        self.root_path = self.__config['save_path']
        self.save_path = os.path.join(self.__config['save_path'], 'corner_poses_data')
        self.save_images_path = os.path.join(self.save_path, 'images')
        self.save_flange_poses_path = os.path.join(self.save_path, 'flange_poses')
        self.save_lhtcp_est_poses_path = os.path.join(self.save_path, 'lhtcp_est_poses')
        self.save_lhtcp_gt_poses_path = os.path.join(self.save_path, 'lhtcp_gt_poses')

        if not os.path.exists(self.save_images_path):
            os.makedirs(self.save_images_path)
        if not os.path.exists(self.save_flange_poses_path):
            os.makedirs(self.save_flange_poses_path)
        if not os.path.exists(self.save_lhtcp_est_poses_path):
            os.makedirs(self.save_lhtcp_est_poses_path)
        if not os.path.exists(self.save_lhtcp_gt_poses_path):
            os.makedirs(self.save_lhtcp_gt_poses_path)
        
        # saved_image_files = [img for img in os.listdir(self.save_path) if os.path.isfile(self.save_path + '/' + img)]
        # self.counter = int(len(saved_image_files) / 2)
        saved_image_files = []
        self.counter = 0
        if self.load_data:
            onlyfiles = next(os.walk(self.save_lhtcp_gt_poses_path))[2] #directory is your directory path as string
            self.counter = len(onlyfiles)
        # Robot setup
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group_name = self.__config['robot_group_name']
        self.group = moveit_commander.MoveGroupCommander(self.group_name)

        self.counter_pose = 0

        # E is tool0 in ROS
        # M = LHTCP model
        # self.T_M_E = np.array([[0., 0., -1., -0.6], 
        #                        [-1., 0., 0., 0.], 
        #                        [0., 1., 0., 1.6],
        #                        [0., 0., 0., 1.]])
        # np.save(os.path.join(self.root_path, 'T_M_E.npy'), self.T_M_E)
        self.T_M_E = np.load(os.path.join(self.root_path, 'T_M_E.npy'))

        self.__rots_xyz = ''

    def callback(self, msg0, msg1):
        global number_of_points
        try:
            image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
            image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')

        except CvBridgeError as e:
            print(e)
        

        image0_corners = image0.copy()
        image1_corners = image1.copy()

        # Scale images
        scale_coeff = 0.5
        dims = (int(image0.shape[1]*scale_coeff), int(image0.shape[0]*scale_coeff))

        scaled_image0 = cv2.resize(image0_corners, dims, interpolation=cv2.INTER_AREA)
        scaled_image1 = cv2.resize(image1_corners, dims, interpolation=cv2.INTER_AREA)

        images_concatenated = np.concatenate((scaled_image0, scaled_image1), axis=1)
        cv2.imshow('Images', images_concatenated)

        key = cv2.waitKey(3)

        if key == 32: # spacebar entered
            cv2.imwrite(self.save_images_path + '/{}-{}.png'.format(self.counter, 0), image0)
            cv2.imwrite(self.save_images_path + '/{}-{}.png'.format(self.counter, 1), image1)
            print('Saved images {}!'.format(self.counter))

            pose_stamped = self.group.get_current_pose()
            T_E_B = self.pose_stamped_to_matrix(pose_stamped)
            joints = self.group.get_current_joint_values()
            joints_array = np.array(joints)

            np.save(os.path.join(self.save_flange_poses_path, 'T_E_B_%d.npy' % self.counter), T_E_B)
            np.save(os.path.join(self.save_flange_poses_path, 'joints_%d.npy' % self.counter), joints_array)

            T_M_B_ = T_E_B @ self.T_M_E
            np.save(os.path.join(self.save_lhtcp_gt_poses_path, 'T_M_B_%d.npy' % self.counter), T_M_B_)
            
            self.counter += 1

        

        if key == ord('o'):
            print('Generating and saving poses in the polygon...')
            plgn, avg_z = self.get_2d_polygon_points()
            t_M_Bs, rots = self.generate_positions_from_polygon(plgn, avg_z)
            self.__rots_xyz = rots
            np.save(os.path.join(self.save_path, 'positions_M_B.npy'), t_M_Bs)
            # np.save(os.path.join(self.save_path, 'T_M_B_rots.npy'), rots_xyz)
            print('Done')

        if key == ord('i'):
            print('Starting initial pose and rotation calc')
            pose_stamped = self.group.get_current_pose()
            T_E_B = self.pose_stamped_to_matrix(pose_stamped)
            np.save(os.path.join(self.save_path, 'T_E_B_init.npy'), T_E_B)

            T_M_B_init = T_E_B @ self.T_M_E

            T_M_B_rotated = np.zeros((number_of_points, 4, 4))
            T_E_B_rotated = np.zeros((number_of_points, 4, 4))
            
            for idx in range(number_of_points):
                Tz = rvl.rotz(np.deg2rad(self.__rots_xyz[idx, 2]))
                Ty = rvl.roty(np.deg2rad(self.__rots_xyz[idx, 1]))
                Tx = rvl.rotx(np.deg2rad(self.__rots_xyz[idx, 0]))
                T_M_B_rotated[idx, :, :] = T_M_B_init @ Tz @ Ty @ Tx
                T_E_B_rotated[idx, :, :] = T_M_B_rotated[idx, :, :] @ np.linalg.inv(self.T_M_E)

            np.save(os.path.join(self.save_path, 'T_M_B_all.npy'), T_M_B_rotated)
            np.save(os.path.join(self.save_path, 'T_E_B_all.npy'), T_E_B_rotated)
            print('Done')

        if key == ord('q'):
            self.image_sub_cam0.sub.unregister()
            self.image_sub_cam1.sub.unregister()
            cv2.destroyAllWindows()
            print('Shutting down')
            rospy.signal_shutdown('Done geenerating.')


    def pose_to_dict(self, pose):
        pose_dict = {
            'position': {
                'x': pose.position.x,
                'y': pose.position.y,
                'z': pose.position.z
            },
            'orientation': {
                'x': pose.orientation.x,
                'y': pose.orientation.y,
                'z': pose.orientation.z,
                'w': pose.orientation.w
            }
        }
        return pose_dict


    def pose_stamped_to_matrix(self, pose: PoseStamped) -> np.ndarray: 
        pose_ = pose.pose
        r_ = pose_.orientation
        t_ = pose_.position
        q_ = [r_.x, r_.y, r_.z, r_.w]
        
        T = quaternion_matrix(q_)
        T[:3, 3] = np.array([t_.x, t_.y, t_.z])
        return T


    def get_2d_polygon_points(self):
        corner_pts = np.zeros(shape=(self.counter , 2), dtype=float)
        
        avg_z = 0
        
        for idx in range(self.counter):
            T_M_B_ = np.load(os.path.join(self.save_lhtcp_gt_poses_path, 'T_M_B_%d.npy' % idx))
            corner_pts[idx, :2] = T_M_B_[:2, 3]
            avg_z += T_M_B_[2, 3]

        avg_z /= (self.counter) 
        
        return Polygon(corner_pts), avg_z


    def generate_positions_from_polygon(self, polygon, avg_z):
        global number_of_points, rot_max_deg, rot_min_deg

        t_M_Bs = np.zeros((number_of_points, 3))
        rots_xyz = np.zeros((number_of_points, 3))

        points = []
        minx, miny, maxx, maxy = polygon.bounds
        pt_count = 0
        while pt_count < number_of_points:
            pnt = Point(np.random.uniform(minx, maxx), np.random.uniform(miny, maxy))
            if polygon.contains(pnt):
                points.append(pnt)
                rots_xyz[pt_count, :] = np.random.uniform(rot_min_deg, rot_max_deg, size=(3,))
                t_M_Bs[pt_count, :] = np.array([pnt.x, pnt.y, avg_z])
                pt_count += 1


        # xp,yp = polygon.exterior.xy
        # plt.plot(xp,yp)
        # xs = [point.x for point in points]
        # ys = [point.y for point in points]
        # plt.scatter(xs, ys,color="red")
        # plt.show()
        
        return t_M_Bs, rots_xyz



def main(args):

    moveit_commander.roscpp_initialize(args)
    rospy.init_node('capture_corner_poses_node', anonymous=True)
    
    rp = RosPack()
    package_path = rp.get_path('lhtcp_detect')
    config_path = os.path.join(package_path, 'config')

    with open(os.path.join(config_path, 'config.yaml'), 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    
    bis = ImagePoseSaver(config)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

