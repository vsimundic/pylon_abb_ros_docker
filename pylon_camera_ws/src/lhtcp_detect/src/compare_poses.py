import numpy as np
import os

np.set_printoptions(suppress=True)
root_path = '/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619'

T_EG_B_working = np.load(os.path.join(root_path, 'T_EG_B_working.npy'))
T_EG_B_broken = np.load(os.path.join(root_path, 'T_EG_B_broken.npy'))

print(T_EG_B_working[:3,3]-T_EG_B_broken[:3,3])

T_T_B_working = np.load(os.path.join(root_path, 'T_T_B_working.npy'))
T_T_B_broken = np.load(os.path.join(root_path, 'T_T_B_broken.npy'))
print(T_T_B_working-T_T_B_broken)
