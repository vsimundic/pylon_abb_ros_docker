#!/usr/bin/env python  
import roslib
import rospy
import math
import tf
import geometry_msgs.msg
import turtlesim.srv
from tf.transformations import quaternion_matrix
import numpy as np
import os

def get_transformation(source_frame, target_frame):
    listener = tf.TransformListener()

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        try:
            # trans, rot = listener.lookupTransform('base_link', 'camera0_optical_link', rospy.Time(0))
            # trans, rot = listener.lookupTransform('camera0_optical_link', 'camera1_optical_link', rospy.Time(0))
            # trans, rot = listener.lookupTransform('base_link', 'lhtcp_link', rospy.Time(0))
            trans, rot = listener.lookupTransform(target_frame, source_frame, rospy.Time(0))
            T = quaternion_matrix(rot)
            T[:3, 3] = np.array(trans)
            # print(T)
            # root_path = '/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619'
            # np.save(os.path.join(root_path, 'T_M_C0_gt.npy'), T)
            # np.save(os.path.join(root_path, 'camera_config', 'T_C0_B.npy'), T)
            return T
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

if __name__ == '__main__':
    rospy.init_node('tf_listener_node')
    np.set_printoptions(suppress=True)    
    listener = tf.TransformListener()

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        try:
            # trans, rot = listener.lookupTransform('base_link', 'camera0_optical_link', rospy.Time(0))
            # trans, rot = listener.lookupTransform('camera0_optical_link', 'camera1_optical_link', rospy.Time(0))
            # trans, rot = listener.lookupTransform('base_link', 'lhtcp_link', rospy.Time(0))
            trans, rot = listener.lookupTransform('base_link', 'goal_hoop_link', rospy.Time(0))
            T = quaternion_matrix(rot)
            T[:3, 3] = np.array(trans)
            print(T)
            # root_path = '/home/RVLuser/data/Exp-LHTCP_deformation_comensation_Gazebo_20240619'
            # np.save(os.path.join(root_path, 'T_M_C0_gt.npy'), T)
            # # np.save(os.path.join(root_path, 'camera_config', 'T_C0_B.npy'), T)
            break
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue