#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import sys
import numpy as np
import message_filters
import os
import yaml
from rospkg import RosPack
import moveit_commander
from glob import glob
from shapely.geometry import Polygon, Point
import matplotlib.pyplot as plt
from tf.transformations import quaternion_matrix
import rvlpyutil as rvl
from trac_ik_python.trac_ik import IK
from tf.transformations import quaternion_from_matrix, quaternion_matrix
from geometry_msgs.msg import Pose, PoseStamped
import rvlpyutil as rvl
import RVLPYDDDetectorLHTCP as rvl


np.random.seed(50)
num_points = 20


class ImagePoseSaver:
    def __init__(self, config):
        
        # Load config from a file
        self.__config = config

        # Flags
        self.is_auto_capture = bool(self.__config['auto_capture'])
        self.with_detection = bool(self.__config['with_detection'])

        # Bridge for ROS messages and CV images
        self.bridge = CvBridge()

        # Subscribers for images
        self.image_sub_cam0 = message_filters.Subscriber(self.__config['camera0_topic'], Image)
        self.image_sub_cam1 = message_filters.Subscriber(self.__config['camera1_topic'], Image)
        self.ts = message_filters.ApproximateTimeSynchronizer([self.image_sub_cam0, self.image_sub_cam1], 100, 10, allow_headerless=True)
        # self.ts.registerCallback(self.callback)

        # Save paths
        self.root_path = self.__config['save_path']
        self.save_path = os.path.join(self.__config['save_path'], 'results')
        self.save_images_path = os.path.join(self.save_path, 'images')
        self.save_images_est_path = os.path.join(self.save_path, 'images_est')
        self.save_flange_poses_path = os.path.join(self.save_path, 'flange_poses')
        self.save_lhtcp_est_in_camera_path = os.path.join(self.save_path, 'lhtcp_est_in_camera')
        self.save_lhtcp_gt_poses_path = os.path.join(self.save_path, 'lhtcp_gt_poses')
        self.corner_poses_data_path = os.path.join(self.root_path, 'corner_poses_data')

        # Create paths if they don't exist
        if not os.path.exists(self.save_images_path):
            os.makedirs(self.save_images_path)
        if not os.path.exists(self.save_images_est_path):
            os.makedirs(self.save_images_est_path)
        if not os.path.exists(self.save_flange_poses_path):
            os.makedirs(self.save_flange_poses_path)
        if not os.path.exists(self.save_lhtcp_est_in_camera_path):
            os.makedirs(self.save_lhtcp_est_in_camera_path)
        if not os.path.exists(self.save_lhtcp_gt_poses_path):
            os.makedirs(self.save_lhtcp_gt_poses_path)
        
        self.counter = 0

        # Robot setup
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group_name = self.__config['robot_group_name']
        self.group = moveit_commander.MoveGroupCommander(self.group_name)
        self.ik = IK('base_link', 'tool0', timeout=0.05, solve_type='Distance')


        self.counter_pose = 0

        # E is tool0 in ROS
        # M = LHTCP model
        self.T_M_E = np.array([[0., 0., -1., -0.6], 
                               [-1., 0., 0., 0.], 
                               [0., 1., 0., 1.6],
                               [0., 0., 0., 1.]])
        np.save(os.path.join(self.root_path, 'T_M_E.npy'), self.T_M_E)
        self.T_M_E = np.load(os.path.join(self.root_path, 'T_M_E.npy'))
        if self.is_auto_capture:
            self.T_E_B_all = np.load(os.path.join(self.corner_poses_data_path, 'T_E_B_all.npy') )
            self.T_E_B_init = np.load(os.path.join(self.corner_poses_data_path, 'T_E_B_init.npy'))


        self.run_detection()

    def run_detection(self):
        global num_points

        # Send robot to initial pose

        if self.is_auto_capture:
            self.send_pose_to_robot(self.T_E_B_init)
        
        joints_init = self.group.get_current_joint_values()
        idx = 0
        idx_T = 0

        while idx < num_points:
            
            if self.is_auto_capture:
                # Send robot to pose

                joints_ = None
                while joints_ is None:
                    T_ = self.T_E_B_all[idx_T, :, :]
                    idx_T += 1
                    joints_ = self.get_inverse_kin(joints_init, T_)

                self.send_joint_values_to_robot(joints_, wait=True)
            else:
                key = 0
                while key != 't':
                    key = input('Adjust pose and press \'t\': ')
            
            # Save robot pose

            T_E_B_current = self.get_current_tool_pose()
            np.save(os.path.join(self.save_flange_poses_path, 'T_E_B_%d.npy' % idx), T_E_B_current)

            # Get images from cameras

            msg0 = rospy.wait_for_message(self.__config['camera0_topic'], Image)
            msg1 = rospy.wait_for_message(self.__config['camera1_topic'], Image)
            image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
            image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')

            # Save images before estimation

            img0_filename = self.save_images_path + '/{}-{}.png'.format(idx, 0)
            img1_filename = self.save_images_path + '/{}-{}.png'.format(idx, 1)
            cv2.imwrite(img0_filename, image0)
            cv2.imwrite(img1_filename, image1)

            if self.with_detection:
                # RVL LHTCP detection
                # RVLDetectorLHTCP
                detector = rvl.PYDDDetectorLHTCP()
                detector.create(self.__config['LHTCP_detector_config_path'])
                detector.load_cameras_parameters(self.__config['cameras_params_dir'])
                detector.load_extrinsic_camera_parameters(self.__config['extrinsic_camera_params_dir'])
                detector.create_lhtcp_model(0.015, 0.1, 0.005, 0.7)
                detector.generate_init_poses(20.0)
                detector.add_rgb_images(img0_filename, img1_filename)
                T_M_C0, solution0, solution1 = detector.detect()

                del detector
                
                # Save estimation results

                np.save(os.path.join(self.save_lhtcp_est_in_camera_path, 'T_M_C0_%d' % idx), T_M_C0)
                solution0_filename = self.save_images_est_path + '/{}-{}.png'.format(idx, 0)
                solution1_filename = self.save_images_est_path + '/{}-{}.png'.format(idx, 1)
                cv2.imwrite(solution0_filename, solution0)
                cv2.imwrite(solution1_filename, solution1)

            idx += 1

            
    def get_current_tool_pose(self):
        pose_ = self.group.get_current_pose()

        return self.__pose_stamped_to_matrix(pose_)


    def get_inverse_kin(self, q_init, T):

        pose_ = self.__matrix_to_pose(T)
        
        q = self.ik.get_ik(q_init, pose_.position.x, 
                       pose_.position.y, 
                       pose_.position.z,
                       pose_.orientation.x, 
                       pose_.orientation.y, 
                       pose_.orientation.z, 
                       pose_.orientation.w)
        if q is None:
            return None
        return list(q)

    def send_joint_values_to_robot(self, joint_values,  wait: bool=True):
        self.group.set_joint_value_target(joint_values)
        self.group.go(wait=wait)
        self.group.stop()

    def send_pose_to_robot(self, T: np.ndarray, wait: bool=True, cartesian: bool=False):

        pose_goal = self.__matrix_to_pose(T)

        if cartesian:
            plan, _ = self.group.compute_cartesian_path([pose_goal], 0.01, 0.0)
            self.group.execute(plan, wait=wait)
            self.group.clear_pose_targets()
            return

        self.group.set_pose_target(pose_goal)
        success = self.group.go(wait=wait)
        self.group.stop()
        self.group.clear_pose_targets()
        return success
    
    def pose_stamped_to_matrix(self, pose: PoseStamped) -> np.ndarray: 
        pose_ = pose.pose
        r_ = pose_.orientation
        t_ = pose_.position
        q_ = [r_.x, r_.y, r_.z, r_.w]
        
        T = quaternion_matrix(q_)
        T[:3, 3] = np.array([t_.x, t_.y, t_.z])


    def get_2d_polygon_points(self):
        corner_pts = np.zeros(shape=(self.counter + 1, 2), dtype=float)
        
        avg_z = 0
        
        for idx in range(self.counter + 1):
            T_M_B_ = np.load(os.path.join(self.save_lhtcp_gt_poses_path, 'T_M_B_%d.npy' % idx))
            corner_pts[idx, :2] = T_M_B_[:2, 3]
            avg_z += T_M_B_[2, 3]

        avg_z /= (self.counter + 1) 
        
        return Polygon(corner_pts), avg_z


    def generate_positions_from_polygon(self, polygon, avg_z):
        global number_of_points, rot_max_deg, rot_min_deg

        t_M_Bs = np.zeros((number_of_points, 3))
        rots_xyz = np.zeros((number_of_points, 3))

        points = []
        minx, miny, maxx, maxy = polygon.bounds
        pt_count = 0
        while pt_count < number_of_points:
            pnt = Point(np.random.uniform(minx, maxx), np.random.uniform(miny, maxy))
            if polygon.contains(pnt):
                points.append(pnt)
                rots_xyz[pt_count, :] = np.random.uniform(rot_min_deg, rot_max_deg, size=(3,))
                t_M_Bs[pt_count, :] = np.array([pnt.x, pnt.y, avg_z])
            
            pt_count += 1


        xp,yp = polygon.exterior.xy
        plt.plot(xp,yp)
        xs = [point.x for point in points]
        ys = [point.y for point in points]
        plt.scatter(xs, ys,color="red")
        plt.show()
        
        return t_M_Bs, rots_xyz
    
    @staticmethod
    def __matrix_to_pose(T: np.ndarray):
        q = quaternion_from_matrix(T) # xyzw

        pose = Pose()
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]
        pose.position.x = T[0, 3]
        pose.position.y = T[1, 3]
        pose.position.z = T[2, 3]
        return pose

    @staticmethod
    def __pose_stamped_to_matrix(pose: PoseStamped):
        pose_ = pose.pose
        q_ = [pose_.orientation.x, pose_.orientation.y, pose_.orientation.z, pose_.orientation.w]
        t_ = np.array([pose_.position.x, pose_.position.y, pose_.position.z])
        T = quaternion_matrix(q_)
        T[:3, 3] = np.array(t_)
        return T



def main(args):

    moveit_commander.roscpp_initialize(args)
    rospy.init_node('lhtcp_detection_from_poses', anonymous=True)
    
    rp = RosPack()
    package_path = rp.get_path('lhtcp_detect')
    config_path = os.path.join(package_path, 'config')

    with open(os.path.join(config_path, 'config.yaml'), 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    
    bis = ImagePoseSaver(config)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

