#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import sys
import numpy as np
import message_filters
import os
import yaml
from rospkg import RosPack
import moveit_commander
from glob import glob
from shapely.geometry import Polygon, Point
import matplotlib.pyplot as plt
from tf.transformations import quaternion_matrix
import rvlpyutil as rvl
from trac_ik_python.trac_ik import IK
from tf.transformations import quaternion_from_matrix, quaternion_matrix
from geometry_msgs.msg import Pose, PoseStamped
import rvlpyutil as rvl
import RVLPYDDDetectorLHTCP as rvl


np.random.seed(50)
num_points = 20


class ImagePoseSaver:
    def __init__(self, config):
        
        # Load config from a file
        self.__config = config

        # Bridge for ROS messages and CV images
        self.bridge = CvBridge()

        # Subscribers for images
        self.image_sub_cam0 = message_filters.Subscriber(self.__config['camera0_topic'], Image)
        self.image_sub_cam1 = message_filters.Subscriber(self.__config['camera1_topic'], Image)
        self.ts = message_filters.ApproximateTimeSynchronizer([self.image_sub_cam0, self.image_sub_cam1], 100, 10, allow_headerless=True)
        # self.ts.registerCallback(self.callback)

        # Save paths
        self.root_path = self.__config['save_path']
        self.save_path = os.path.join(self.__config['save_path_camera_config'])
        self.save_images_path = os.path.join(self.save_path, 'images')

        # Create paths if they don't exist
        if not os.path.exists(self.save_images_path):
            os.makedirs(self.save_images_path)

        self.counter = 0
            
    def callback(self, msg0, msg1):
        try:
            image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
            image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')

        except CvBridgeError as e:
            print(e)
        

        image0_corners = image0.copy()
        image1_corners = image1.copy()

        # Scale images
        scale_coeff = 0.5
        dims = (int(image0.shape[1]*scale_coeff), int(image0.shape[0]*scale_coeff))

        scaled_image0 = cv2.resize(image0_corners, dims, interpolation=cv2.INTER_AREA)
        scaled_image1 = cv2.resize(image1_corners, dims, interpolation=cv2.INTER_AREA)

        images_concatenated = np.concatenate((scaled_image0, scaled_image1), axis=1)
        cv2.imshow('Images', images_concatenated)

        key = cv2.waitKey(3)

        if key == 32: # spacebar entered
            cv2.imwrite(self.save_images_path + '/{}-{}.png'.format(self.counter, 0), image0)
            cv2.imwrite(self.save_images_path + '/{}-{}.png'.format(self.counter, 1), image1)
            print('Saved images {}!'.format(self.counter))

            self.counter += 1

        if key == ord('q'):
            self.image_sub_cam0.sub.unregister()
            self.image_sub_cam1.sub.unregister()
            cv2.destroyAllWindows()
            print('Shutting down')
            rospy.signal_shutdown('Done geenerating.')


def main(args):

    moveit_commander.roscpp_initialize(args)
    rospy.init_node('image_capture_node', anonymous=True)
    
    rp = RosPack()
    package_path = rp.get_path('lhtcp_detect')
    config_path = os.path.join(package_path, 'config')

    with open(os.path.join(config_path, 'config.yaml'), 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    
    bis = ImagePoseSaver(config)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

