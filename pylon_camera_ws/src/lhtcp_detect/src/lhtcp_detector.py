import numpy as np
import rvlpyutil as rvl
import RVLPYDDDetectorLHTCP as rvl_lhtcp_detector

class LHTCPDetector:
    def __init__(self, config_path: str, intrinsic_camera_params_path: str, extrinsic_camera_params_path: str) -> None:
        self.detector = rvl_lhtcp_detector.PYDDDetectorLHTCP()
        self.detector.create(config_path)
        self.detector.load_cameras_parameters(intrinsic_camera_params_path)
        self.detector.load_extrinsic_camera_parameters(extrinsic_camera_params_path)
        self.detector.create_lhtcp_model(0.015, 0.1, 0.005, 0.7)
        self.detector.generate_init_poses(20.0)


    def detect(self, img0_filename: str, img1_filename: str):
        self.detector.add_rgb_images(img0_filename, img1_filename)

        T_M_C0, solution0, solution1 = self.detector.detect()

        return T_M_C0, solution0, solution1
    
    def visualize_from_pose(self, T_M_C0: np.ndarray, T_M_C1: np.ndarray):
        T_M_C0_ = T_M_C0.copy()
        T_M_C1_ = T_M_C1.copy()
        self.detector.visualize_from_pose(T_M_C0_, T_M_C1_)