#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import sys
import numpy as np
import message_filters
import os
import yaml
from rospkg import RosPack

class BaslerImageSaver:
    def __init__(self, config):
        self.__config = config
        self.bridge = CvBridge()

        self.image_sub_cam0 = message_filters.Subscriber(self.__config['camera0_topic'], Image)
        self.image_sub_cam1 = message_filters.Subscriber(self.__config['camera1_topic'], Image)

        ts = message_filters.ApproximateTimeSynchronizer([self.image_sub_cam0, self.image_sub_cam1], 100, 10, allow_headerless=True)
        ts.registerCallback(self.callback)

        self.save_path = self.__config['save_path']
        try:
            # Create target Directory
            os.makedirs(self.save_path)
            print('Directory ' + self.save_path +  ' created.') 
        except Exception:
            print('Directory ' + self.save_path +  ' already exists.')


        saved_image_files = [img for img in os.listdir(self.save_path) if os.path.isfile(self.save_path + '/' + img)]
        self.counter = int(len(saved_image_files) / 2)


    def callback(self, msg0, msg1):
        try:
            image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
            image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')

        except CvBridgeError as e:
            print(e)
        

        image0_corners = image0.copy()
        image1_corners = image1.copy()
        # image0_corners = cv2.cvtColor(image0, cv2.COLOR_RGB2GRAY)
        # image1_corners = cv2.cvtColor(image1, cv2.COLOR_RGB2GRAY)

        # ret0, corners0 = cv2.findChessboardCorners(image0_corners, (8, 6), None)
        # ret1, corners1 = cv2.findChessboardCorners(image1_corners, (8, 6), None)

        # if ret0 == True:
        #     cv2.drawChessboardCorners(image0_corners, (8, 6), corners0, ret0)
        # if ret1 == True:
        #     cv2.drawChessboardCorners(image1_corners, (8, 6), corners1, ret1)

        # Scale images
        scale_coeff = 0.5
        dims = (int(image0.shape[1]*scale_coeff), int(image0.shape[0]*scale_coeff))

        scaled_image0 = cv2.resize(image0_corners, dims, interpolation=cv2.INTER_AREA)
        scaled_image1 = cv2.resize(image1_corners, dims, interpolation=cv2.INTER_AREA)

        images_concatenated = np.concatenate((scaled_image0, scaled_image1), axis=1)
        cv2.imshow('Images', images_concatenated)

        key = cv2.waitKey(3)

        if key == 32: # spacebar entered
            cv2.imwrite(self.save_path + '/{}-{}.png'.format(self.counter, 0), image0)
            cv2.imwrite(self.save_path + '/{}-{}.png'.format(self.counter, 1), image1)
            print('Saved images {}!'.format(self.counter))
            self.counter += 1


def main(args):
    rospy.init_node('basler_image_capture_node')
    
    rp = RosPack()
    package_path = rp.get_path('basler_capture_images')
    config_path = os.path.join(package_path, 'config')

    with open(os.path.join(config_path, 'config.yaml'), 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    
    bis = BaslerImageSaver(config)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

