#!/usr/bin/env python3

import rospy
import numpy as np
import moveit_commander
from tf.transformations import euler_from_quaternion

np.random.seed(69)
number_of_points = 20

rospy.init_node('capture_images_and_pose_node', anonymous=True)

# Robot setup
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = 'manipulator'
move_group = moveit_commander.MoveGroupCommander(group_name)

pose = move_group.get_current_pose().pose
print(pose)
quaternion = (
    pose.orientation.x,
    pose.orientation.y,
    pose.orientation.z,
    pose.orientation.w
)

euler_angles = euler_from_quaternion(quaternion)
roll, pitch, yaw = euler_angles
print("Roll: {:.2f} degrees, Pitch: {:.2f} degrees, Yaw: {:.2f} degrees".format(
    np.degrees(roll), np.degrees(pitch), np.degrees(yaw)
))

angle = np.degrees(yaw)
# angle = (angle + 180.) % 360. - 180.
print(angle)
# # def main(args):

# #     moveit_commander.roscpp_initialize(args)
# #     try:
# #         rospy.spin()
# #     except KeyboardInterrupt:
# #         print('Shutting down')

# #     cv2.destroyAllWindows()

# # if __name__ == '__main__':
# #     # polygon = get_polygon_points('/home/pylon_user/pylon_camera_ws/data/test/corner_poses')
# #     # generate_points_from_polygon(polygon)
# #     main(sys.argv)

