#!/usr/bin/env python3

import rospy
import tf
from tf.transformations import quaternion_from_matrix, euler_from_quaternion
import numpy as np

TAT = np.array([[0., 0., -1., -0.6], 
               [-1., 0., 0., 0.], 
               [0., 1., 0., 1.6],
               [0., 0., 0., 1.]])

print(TAT)
q = quaternion_from_matrix(TAT)

w=0.5598229377994167
x=0.4353671537272773
y=0.5489694743847365
z=-0.442364370132570

print(euler_from_quaternion([x, y, z, w]))

if __name__ == '__main__':
    
    rospy.init_node('fixed_tf_broadcaster')
    br = tf.TransformBroadcaster()
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        br.sendTransform(TAT[:3, 3],
                         q,
                         rospy.Time.now(),
                         "tcp",
                         "tool0")
        rate.sleep()