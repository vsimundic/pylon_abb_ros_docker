#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import sys
import numpy as np
import message_filters
import os
import yaml
from rospkg import RosPack
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from glob import glob
from shapely.geometry import Polygon, Point
import matplotlib.pyplot as plt

np.random.seed(69)
number_of_points = 20




class ImagePoseSaver:
    def __init__(self, config):
        self.__config = config
        self.bridge = CvBridge()

        self.image_sub_cam0 = message_filters.Subscriber(self.__config['camera0_topic'], Image)
        self.image_sub_cam1 = message_filters.Subscriber(self.__config['camera1_topic'], Image)

        ts = message_filters.ApproximateTimeSynchronizer([self.image_sub_cam0, self.image_sub_cam1], 100, 10, allow_headerless=True)
        ts.registerCallback(self.callback)

        self.save_path = self.__config['save_path']
        self.save_images_path = os.path.join(self.save_path, 'images')
        self.save_poses_path = os.path.join(self.save_path, 'poses')
        self.save_corner_poses_path = os.path.join(self.save_path, 'corner_poses')

        if not os.path.exists(self.save_images_path):
            os.makedirs(self.save_images_path)
        if not os.path.exists(self.save_poses_path):
            os.makedirs(self.save_poses_path)
        if not os.path.exists(self.save_corner_poses_path):
            os.makedirs(self.save_corner_poses_path)

        saved_image_files = [img for img in os.listdir(self.save_path) if os.path.isfile(self.save_path + '/' + img)]
        self.counter = int(len(saved_image_files) / 2)


        # Robot setup
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group_name = self.__config['robot_group_name']
        self.group = moveit_commander.MoveGroupCommander(self.group_name)

        self.counter_pose = 0


        # # Points
        # self.polygon = self.get_polygon_points()
        # self.points_in_polygon = self.generate_points_from_polygon(self.polygon)


    def callback(self, msg0, msg1):
        try:
            image0 = self.bridge.imgmsg_to_cv2(msg0, 'mono8')
            image1 = self.bridge.imgmsg_to_cv2(msg1, 'mono8')

        except CvBridgeError as e:
            print(e)
        

        image0_corners = image0.copy()
        image1_corners = image1.copy()

        # Scale images
        scale_coeff = 0.5
        dims = (int(image0.shape[1]*scale_coeff), int(image0.shape[0]*scale_coeff))

        scaled_image0 = cv2.resize(image0_corners, dims, interpolation=cv2.INTER_AREA)
        scaled_image1 = cv2.resize(image1_corners, dims, interpolation=cv2.INTER_AREA)

        images_concatenated = np.concatenate((scaled_image1, scaled_image0), axis=1)
        cv2.imshow('Images', images_concatenated)

        key = cv2.waitKey(3)

        if key == 32: # spacebar entered
            cv2.imwrite(self.save_images_path + '/{}-{}.png'.format(self.counter, 0), image0)
            cv2.imwrite(self.save_images_path + '/{}-{}.png'.format(self.counter, 1), image1)
            print('Saved images {}!'.format(self.counter))

            pose = self.group.get_current_pose().pose
            pose_ = self.pose_to_dict(pose)
            joints = self.group.get_current_joint_values()
            save_data = pose_.copy()
            save_data['joints'] = joints
            with open(os.path.join(self.save_poses_path, '{}.yaml'.format(self.counter)), 'w') as f:
                yaml.dump(save_data, f)
            
            self.counter += 1

        if key == ord('p'):
            pose = self.group.get_current_pose().pose
            pose_ = self.pose_to_dict(pose)
            joints = self.group.get_current_joint_values()
            save_data = pose_.copy()
            save_data['joints'] = joints
            with open(os.path.join(self.save_corner_poses_path, '{}.yaml'.format(self.counter_pose)), 'w') as f:
                yaml.dump(save_data, f)
            
            self.counter_pose += 1


    def pose_to_dict(self, pose):
        pose_dict = {
            'position': {
                'x': pose.position.x,
                'y': pose.position.y,
                'z': pose.position.z
            },
            'orientation': {
                'x': pose.orientation.x,
                'y': pose.orientation.y,
                'z': pose.orientation.z,
                'w': pose.orientation.w
            }
        }
        return pose_dict


    def get_polygon_points(self):
        corner_pts = np.zeros(shape=(5, 2), dtype=float)
        for idx, corner_pt_file in enumerate(sorted(glob(self.__config['corner_points_path'] + '/*.yaml'))):
            with open(corner_pt_file) as f:
                corner_pt = yaml.safe_load(f)
            corner_pts[idx, :2] = np.array([corner_pt['position']['x'], corner_pt['position']['y']])
        
        return Polygon(corner_pts)


    def generate_points_from_polygon(self, polygon):
        global number_of_points
    
        points = []
        minx, miny, maxx, maxy = polygon.bounds
        while len(points) < number_of_points:
            pnt = Point(np.random.uniform(minx, maxx), np.random.uniform(miny, maxy))
            if polygon.contains(pnt):
                points.append(pnt)
        
        xp,yp = polygon.exterior.xy
        plt.plot(xp,yp)
        xs = [point.x for point in points]
        ys = [point.y for point in points]
        plt.scatter(xs, ys,color="red")
        plt.show()
        
        return points



def main(args):

    moveit_commander.roscpp_initialize(args)
    rospy.init_node('capture_images_and_pose_node', anonymous=True)
    
    rp = RosPack()
    package_path = rp.get_path('capture_images_and_pose')
    config_path = os.path.join(package_path, 'config')

    with open(os.path.join(config_path, 'config.yaml'), 'r') as fcfg:
        config = yaml.load(fcfg, Loader=yaml.FullLoader)

    
    bis = ImagePoseSaver(config)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print('Shutting down')
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    # polygon = get_polygon_points('/home/pylon_user/pylon_camera_ws/data/test/corner_poses')
    # generate_points_from_polygon(polygon)
    main(sys.argv)

