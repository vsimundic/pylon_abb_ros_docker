# PYLON-ROS CAMERA WORKSPACE

## Installation
1. Open terminal in `pylon_camera_ws` directory and type:
```bash
./build_docker
./run_docker
```

2. In the container, type:
```bash
chmod +x init.sh
./init.sh
```

## Usage
2. Source and run drivers for cameras:
```bash
source devel/setup.bash
roslaunch pylon_camera pylon_2_cameras_nodes_MONO.launch
```
3. In `pylon_camera_ws/src/basler_capture_images/config/config.yaml`, you can change your desired save path. 

4. Run this in another terminal:
```bash
roslaunch abb_irb2400_moveit_config moveit_planning_execution.launch robot_ip:=192.168.20.40
```

5. In another terminal, `source` again and type:
```bash
source devel/setup.bash
rosrun capture_images_and_pose capture_images_and_pose.py
```

6. You should see two concatenated images in one window, one from each camera. Press `space` while your window is active to save images. Press `p` to save a corner pose.

