docker stop ros_pylon
docker rm ros_pylon
docker run --ipc=host --gpus all --runtime=runc --interactive -it \
--shm-size=10gb \
--env="DISPLAY" \
--volume="${PWD}:/home/RVLuser" \
--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
--volume="/dev:/dev" \
--workdir="/home/RVLuser" \
--privileged \
--network host \
--name=ros_pylon \
ros_pylon:rvl
# docker exec -it ros_pylon:rvl bash
