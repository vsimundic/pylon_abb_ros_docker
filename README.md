# LHTCP project docker container

## Installation
1. Build the container:
```bash
./build_docker.sh
```
2. Run the container:
```bash
./run_docker.sh
```
3. Install dragandbot i pylon-ros-camera
```bash
sh -c 'echo "yaml https://raw.githubusercontent.com/basler/pylon-ros-camera/master/pylon_camera/rosdep/pylon_sdk.yaml" > /etc/ros/rosdep/sources.list.d/30-pylon_camera.list' && rosdep update && sudo rosdep install --from-paths . --ignore-src --rosdistro=$ROS_DISTRO -y
```
```bash
cd ~/catkin_ws && catkin clean -y && catkin build && source ~/.bashrc
```

## Usage
1. Source and run cameras in pylon_camera_ws:
```bash
source devel/setup.bash
roslaunch pylon_camera pylon_2_cameras_nodes_MONO.launch
```
2. Run robot stuff in catkin_abb_ws:
```bash
source devel/setup.bash
roslaunch irb2400_moveit_config moveit_planning_execution.launch robot_ip:=192.168.20.40
```