# ABB IRB 2400 Gazebo

##Overview

This package contains the files required to simulate the ABB IRB 2400  manipulator (and variants) in Gazebo. 

Current version supports ROS Indigo, but can be modified for ROS Hydro as noted below.


## Using Moveit! with Gazebo Simulator

1. Bring the robot model into gazebo and load the ros_control controllers:
   ```roslaunch irb2400_gazebo irb2400_gazebo.launch``` 

2. Launch moveit! and ensure that it is configured to run alongside Gazebo:
```roslaunch irb2400_moveit_config moveit_planning_execution_gazebo.launch``` 
