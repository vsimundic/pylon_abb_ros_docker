FROM nvidia/cuda:11.1.1-cudnn8-devel-ubuntu20.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    build-essential libudev-dev usbutils libcanberra-gtk-module libcanberra-gtk3-module openjdk-8-jdk freeglut3-dev libusb-1.0-0-dev g++ curl \
    protobuf-compiler libprotoc-dev python3-opencv ca-certificates python3-dev python3-pip git wget sudo  \
    cmake ninja-build 
ENV NO_AT_BRIDGE=1

RUN ln -sf /usr/bin/python3 /usr/bin/python

###### RVL ######

RUN apt install -y libeigen3-dev
RUN apt-get install -y cmake-curses-gui
RUN apt-get update
RUN apt-get install -y libgl1-mesa-dev libgl1-mesa-glx xvfb
RUN apt-get update
RUN apt-get install -y gdb libgtk2.0-dev pkg-config
RUN apt-get install -y libhdf5-serial-dev
RUN apt-get update
RUN apt-get install -y libusb-1.0-0-dev libudev-dev
RUN apt-get install -y default-jdk openjdk-11-jdk
RUN apt-get install -y libtiff-dev freeglut3-dev doxygen graphviz

WORKDIR /
# Install VTK
RUN wget https://gitlab.kitware.com/vtk/vtk/-/archive/v7.1.1/vtk-v7.1.1.tar.gz
RUN tar -xf vtk-v7.1.1.tar.gz
RUN apt install -y libgl1-mesa-dev libxt-dev
RUN cd vtk-v7.1.1 && mkdir build && cd build && cmake -DBUILD_TESTING=OFF .. && make -j$(nproc) && make install

# Install OpenCV
RUN wget https://github.com/opencv/opencv/archive/3.4.16.zip
# RUN mkdir /opencv-3.4.16
RUN apt-get install -y unzip
RUN unzip 3.4.16.zip
RUN git clone --depth 1 --branch '3.4.16' https://github.com/opencv/opencv_contrib.git
RUN ls /opencv_contrib
WORKDIR /opencv-3.4.16
RUN mkdir build && cd build && cmake -DOPENCV_EXTRA_MODULES_PATH=/opencv_contrib/modules -DWITH_EIGEN=ON -DWITH_VTK=ON -DBUILD_opencv_world=ON .. && make -j$(nproc) && make install

WORKDIR /
RUN git clone --depth 1 --branch '1.8.4' https://github.com/flann-lib/flann.git
RUN cd flann && touch src/cpp/empty.cpp && sed -e '/add_library(flann_cpp SHARED/ s/""/empty.cpp/' \
    -e '/add_library(flann SHARED/ s/""/empty.cpp/' \
    -i src/cpp/CMakeLists.txt
RUN cd flann && mkdir build && cd build && cmake .. && make -j$(nproc) && make install

#OpenNI
RUN apt-get install -y libopenni-dev
#OpenNI2
RUN apt-get install -y libopenni2-dev

RUN echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu:/home/RVLuser/rvl-linux/build/lib" >> /etc/bash.bashrc

RUN apt-get update
RUN apt-get -y install python3-pip
RUN pip3 install numpy

# Install pybind 11
RUN pip3 install pybind11
RUN apt-get update

ENV PYTHONPATH="${PYTHONPATH}:/home/RVLuser/rvl-linux/python/build/lib"
ENV PYTHONPATH="${PYTHONPATH}:/home/RVLuser/rvl-linux/modules/RVLPY"
ENV PYTHONPATH="${PYTHONPATH}:/home/RVLuser/rvl-linux/python"

# PIP install
RUN pip install numpy opencv-python scipy scikit-image tqdm open3d
RUN pip install matplotlib==1.20

RUN apt-get install -y lsb-release
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt install curl
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
RUN apt update
RUN apt install -y ros-noetic-desktop-full
RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN  apt install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential

RUN rosdep init
RUN rosdep update

RUN apt-get install -y ros-noetic-realsense2-camera
RUN apt-get install -y ros-noetic-openni-launch
RUN apt-get install -y ros-noetic-openni2-launch
# RUN apt-get install -y ros-noetic-ros-numpy
RUN apt-get install -y ros-noetic-rosbash
RUN apt-get install -y ros-noetic-ros-control
RUN apt-get install -y ros-noetic-soem
RUN apt-get install -y ros-noetic-moveit
RUN apt-get install -y ros-noetic-trac-ik
RUN apt-get install -y ros-noetic-industrial-core
RUN apt-get install -y ros-noetic-joint-trajectory-controller
RUN apt-get install -y python3-catkin-tools
RUN pip install ur-rtde
RUN apt-get update

# ENV PYTHONPATH="${PYTHONPATH}:/home/RVLuser/ur5_ws/"

# INSTALL PYLON
RUN mkdir -p /pylon_6.3
COPY pylon_camera_ws/pylon_6.3/ /pylon_6.3
WORKDIR /pylon_6.3
RUN apt install -y ./pylon_*.deb
RUN ls /opt/pylon
ENV PYLON_ROOT='/opt/pylon'
RUN /bin/bash -c 'echo $PYLON_ROOT'
RUN mkdir -p /home/RVLuser/pylon_camera_ws/src
COPY pylon_camera_ws/src/ /home/RVLuser/pylon_camera_ws/src/
WORKDIR /home/RVLuser/pylon_camera_ws


RUN echo 'source /home/RVLuser/catkin_abb_ws/devel/setup.bash --extend' >> /root/.bashrc

# RUN sh -c 'echo "yaml https://raw.githubusercontent.com/basler/pylon-ros-camera/master/pylon_camera/rosdep/pylon_sdk.yaml" > /etc/ros/rosdep/sources.list.d/30-pylon_camera.list'
# RUN rosdep update 

# RUN  /bin/bash -c '. /opt/ros/noetic/setup.bash' &&  rosdep install --from-paths . --ignore-src --rosdistro=$ROS_DISTRO -y

# # WORKDIR /home/RVLuser/pylon_camera_ws
# # RUN /bin/bash -c '. /opt/ros/noetic/setup.bash; catkin_make clean; catkin_make'